setwd("~/Documents/neighbour-pressure")
data <- read.csv('neighbour_pressure.csv')
# data <- read.csv('neighbour_pressure_same_phylum.csv')

feature_counts = xtabs(feature_present ~ feature_name, data = data)
popular.segments = names(feature_counts[which(feature_counts >= 20)])
print(length(popular.segments))
data <- data[which(data$feature_name %in% popular.segments),]

dim(data)

summary(data)

positives <- data[which(data$feature_present == 1),]
negatives <- data[which(data$feature_present == 0),]

par(mfrow = c(1,2))
hist(positives$density, main = 'Density histogram for phonemes\nfound in a given inventory',
     xlab = 'Density', ylab = 'Count')
hist(negatives$density, main = 'Density histogram for phonemes\nnot found in a given inventory',
     xlab = 'Density', ylab = 'Count')

general.density.fit <- glm(feature_present ~ density, family = 'binomial', data = data)
summary(general.density.fit)

MAJOR_FAMILIES = c('Mongolic',
                   'Tai-Kadai',
                   'Austronesian',
                   'Nakh-Daghestanian',
                   'Turkic',
                   'Dravidian',
                   'Uralic',
                   'Mon-Khmer',
                   'Sino-Tibetan',
                   'Indo-European')

# Not all feature combinations are found in the languages
# of these families. Moreover, some features are found
# in almost languages of the sample (431 at this moment),
# and they are also hard to predict.
feature.popularity.major.families <- xtabs(feature_present ~ feature_name,
                                           data = data[which(data$phylum %in% MAJOR_FAMILIES),])
features.from.major.families <- names(feature.popularity.major.families[which(feature.popularity.major.families > 0 &
                                                                              feature.popularity.major.families < 400)])
data.major <- data[which(data$phylum %in% MAJOR_FAMILIES &
                         data$feature_name %in% features.from.major.families),]
data.major <- droplevels(data.major)

summary(data.major)

sort(xtabs(feature_present~feature_name, data = data.major),
     decreasing = T)

require(pROC)

# Suppress warnings for now
oldw <- getOption("warn")
options(warn = -1)

results <- c()
for (q in unique(data.major$feature_name)) {
#     q <- sample(data.major$feature_name, size = 1)
#     q <- 'uvular plosive voiceless'
    print(q)
    d <- data.major[which(data.major$feature_name == q),]
    d <- droplevels(d)
    
    densities.cv.results <- data.frame('response' = c(),
                                       'prediction' = c())
    phylum.cv.results <- data.frame('response' = c(),
                                    'prediction' = c())
    both.cv.results <- data.frame('response' = c(),
                                   'prediction' = c())
    # Leave-one-out cross-validation
    # with ROC-AUC metric
    for (i in 1:nrow(d)) {
        train.data <- d[-i,]
        test.data <- d[i,]
        response <- test.data$feature_present
        fit.densities <- glm(feature_present ~ density,
                         family = 'binomial', data = d)
        fit.phylum <- glm(feature_present ~ phylum,
                      family = 'binomial', data = d)
        fit.both <- glm(feature_present ~ density + phylum,
                    family = 'binomial', data = d)
        densities.cv.results <- rbind(densities.cv.results,
                                     data.frame('response' = response,
                                                'prediction' = predict(fit.densities, test.data)))
        phylum.cv.results <- rbind(phylum.cv.results,
                                  data.frame('response' = response,
                                            'prediction' = predict(fit.phylum, test.data)))
        both.cv.results <-rbind(both.cv.results,
                               data.frame('response' = response,
                                         'prediction' = predict(fit.both, test.data)))
    }
    densities.roc <- roc(response~prediction, data = densities.cv.results)
    phylum.roc <- roc(response~prediction, data = phylum.cv.results)
    both.roc <- roc(response~prediction, data = both.cv.results)
    results <- c(results, sprintf("densities: %.3f, phylum: %.3f, both: %.3f (%s)",
                                  auc(densities.roc),
                                  auc(phylum.roc),
                                  auc(both.roc),
                                  q))
}
# Resume receiving warnings
options(warn = oldw)

# Plots for "uvular plosive voiceless"
par(mfrow = c(2,2))
plot(densities.roc)
plot(phylum.roc)
plot(both.roc)

print(auc(densities.roc))
print(auc(phylum.roc))
print(auc(both.roc))

data <- data.major
performance.df <- data.frame('lang' = c(), 'seg.count' = c(),
                             'true.positives' = c(), 'true.negatives' = c(),
                             'false.positives' = c(), 'false.negatives' = c())
all.langs <- unique(data$lang)
for (i in 1:length(all.langs)) {
    l <- all.langs[i]
    lang.indexes <- which(data$lang == l)
    train.data <- data[-lang.indexes,]
    test.data <- data[lang.indexes,]
    # local.fit <- glm(feature_present ~ density, family = 'binomial', data = train.data)
    local.fit <- glm(feature_present ~ phylum, family = 'binomial', data = train.data)
    predicted.probs <- predict(local.fit, newdata = test.data, type = 'response')
    predicted.segments <- round(predicted.probs)
    true.segments <- test.data$feature_present
    segment.count <- sum(true.segments)
    true.positives <- 0; true.negatives <- 0
    false.positives <- 0; false.negatives <- 0
    for (j in 1:length(true.segments)) {
        predicted.val <- predicted.segments[j]
        true.val <- true.segments[j]
        if (predicted.val == 1 && true.val == 1) {
            true.positives = true.positives + 1
        } else if (predicted.val == 1 && true.val == 0) {
            false.positives = false.positives + 1
        } else if (predicted.val == 0 && true.val == 1) {
            false.negatives = false.negatives + 1
        } else {
            true.negatives = true.negatives + 1
        }
    }
    performance.df <- rbind(performance.df, data.frame('lang' = c(as.character(l)),
                                                       'seg.count' = c(segment.count),
                                                       'true.positives' = c(true.positives),
                                                       'true.negatives' = c(true.negatives),
                                                       'false.positives' = c(false.positives),
                                                       'false.negatives' = c(false.negatives)))
}

require(reshape2)

library(ggplot2)
# ggplot(data = melt(performance.df[,c('true.positives', 'false.positives', 'true.negatives', 'false.negatives')]),
#        mapping = aes(x = value)) + geom_histogram(bins = 20) + facet_wrap(~variable, scales = 'free_x')

new.precision.scores <- performance.df$true.positives / (performance.df$true.positives +
                                                     performance.df$false.positives)
new.recall.scores <- performance.df$true.positives / (performance.df$true.positives +
                                                  performance.df$false.negatives)
new.f1.scores <- 2 * (precision.scores * recall.scores) / (precision.scores + recall.scores)

x_lim <- c(0,1)
y_lim <- c(0, 170)

par(mfrow = c(2,3))
hist(precision.scores, xlim = x_lim, ylim = y_lim)
hist(recall.scores, xlim = x_lim, ylim = y_lim)
hist(f1.scores, xlim = x_lim, ylim = y_lim)

hist(new.precision.scores, xlim = x_lim, ylim = y_lim)
hist(new.recall.scores, xlim = x_lim, ylim = y_lim)
hist(new.f1.scores, xlim = x_lim, ylim = y_lim)

black.sheep <- which(performance.df$true.positives <= 5)
all.langs[black.sheep]

blackest.sheep <- which(performance.df$true.positives < 2)
all.langs[blackest.sheep]
