%pylab
%matplotlib inline
import pandas as pd
d = pd.read_csv('bin_data.csv')
d.shape
d.head()
sum(d['feature_present'])
d.shape
sum(d['feature_present']) / d.shape[0]
round(sum(d['feature_present']) / d.shape[0], 2)
from sklearn.preprocessing import OneHotEncoder
oh_enc = OneHotEncoder()
?oh_enc
from sklearn.ensemble import RandomForestClassifier
rf_clf = RandomForestClassifier()
X = dat.iloc[:,:-1]
X = d.iloc[:,:-1]
X.shape
rowns = list(range(d.shape[0]))
test_ids = random.choice(rowns, len(rowns)//10, replace=False)
len(test_ids)
train_ids = [el for el in rowns if el not in test_ids]
len(train_ids)
train_d = d.loc[traind_ids]
train_d = d.loc[train_ids]
test_d = d.loc[test_ids]
X_train = train_d[:,:-1]; y_train = train_d['feature_present']
X_train = train_d.iloc[:,:-1]; y_train = train_d['feature_present']
X_test = test_d.iloc[:,:-1]; y_test = test_d['feature_present']
rf_clf.fit(X_train, y_train)
from sklearn_pandas import DataFrameMapper
mapper = DataFrameMapper(
    [
        (['phylum', 'genus', 'feature_name'], sklearn.prepocessing.LabelBinarizer())
    ], 
    default=None)
from sklearn.preprocessing import LabelBinarizer
mapper = DataFrameMapper(
    [
        (['phylum', 'genus', 'feature_name'], LabelBinarizer())
    ], 
    default=None)
X = dat.iloc[:,1:-1]; y = dat['feature_present']
X = d.iloc[:,1:-1]; y = d['feature_present']
X_mm = mapper.fit_transform(X.copy(), 1)
X['phylum'][:10]
X['genus'][:10]
X['feature_name'][:10]
mapper = DataFrameMapper(
    [
        ('phylum', LabelBinarizer()),
        ('genus', LabelBinarizer()),
        ('feature_name', LabelBinarizer())
    ], 
    default=None)
X_mm = mapper.fit_transform(X.copy(), 1)
X_mm = mapper.fit_transform(X.fillna('').copy(), 1)
head(X_mm)
X_mm.head()
X_mm[:10,:10]
X_mm.shape
train_d = X_mm[train_ids,]
test_d = X_mm[test_ids,]
train_y = d['feature_present'][train_ids]
test_y = d['feature_present'][test_ids]
rf_clf.fit(train_d, train_y)
from sklearn.metrics import accuracy_score
accuracy_score(test_y, rf_clf.predict(test_d))
import tensorflow as tf
train_input_fn = tf.estimator.inputs.numpy_input_fn(
    x = {'x': train_d},
    y = train_y,
    num_epochs = None,
    shuffle = True)
tf_clf = tf.estimator.DNNClassifier()
feature_columns = tf.contrib.learn.infer_real_valued_columns_from_input(train_d)
train_d.shape
mean([364, 2])
183//2
91//2
45//2
22//2
11//
11//2
tf_clf = tf.estimator.DNNClassifier(feature_columns = feature_columns, hidden_units=[183,91,45,22,11,5])
tf_clf.train(input_fn=train_input_fn, epochs = 10**4)
tf_clf.train(input_fn=train_input_fn, steps = 10**4)
feature_columns = tf.contrib.learn.infer_real_valued_columns_from_input_fn(train_input_fn)
feature_columns
tf_clf = tf.estimator.DNNClassifier(feature_columns = feature_columns, hidden_units=[183,91,45,22,11,5])
tf_clf.train(input_fn=train_input_fn, steps = 10**4)
all(train_y > 0)
all(train_y >= 0)
all(train_d >= 0)
train_input_fn = tf.estimator.inputs.numpy_input_fn(
    x = {'x': train_d},
    y = train_y,
    num_epochs = None,
    shuffle = True)
feature_columns = tf.contrib.learn.infer_real_valued_columns_from_input_fn(train_input_fn)
train_d.shape
type(train_d)
tf_clf = tf.estimator.DNNClassifier(feature_columns = feature_columns, hidden_units=[183,91,45,22,11,5])
tf_clf.train(input_fn=train_input_fn, steps = 10**4)
type(train_y)
type(train_d)
type(train_y.as_matrix)
type(train_y.as_matrix())
train_input_fn = tf.estimator.inputs.numpy_input_fn(
    x = {'x': train_d},
    y = train_y.as_matrix(),
    num_epochs = None,
    shuffle = True)
tf_clf.train(input_fn=train_input_fn, steps = 10**4)
test_input_fn = tf.estimator.inputs.numpy_input_fn(
    x = {'x': test_d},
    y = test_y.as_matrix(),
    num_epochs = None,
    shuffle = False)
predicted_labels = tf_clf.predict(test_input_fn)
predicted_labels[0]
tf.metrics.accuracy(test_y.as_matrix(), predicted_labels)
predicted_labels = list(tf_clf.predict(test_input_fn))
tf_clf.evaluate(input_fn=test_input_fn)
test_input_fn = tf.estimator.inputs.numpy_input_fn(
    x = {'x': test_d},
    y = test_y.as_matrix(),
    num_epochs = 1,
    shuffle = False)
tf_clf.evaluate(input_fn=test_input_fn)
accuracy_score
accuracy_score(test_y, rf_clf.predict(test_d))
from sklearn.metrics import roc_auc_score
roc_auc_score(test_y, rf_clf.predict(test_d))
from sklearn import metrics
fpr, tpr, _ = metrics.roc_curve(test_y, rf_clf.predict(test_d))
metrics.auc(fpr, tpr)
rf_clf = RandomForestClassifier(50)
rf_clf.fit(train_d, train_y)
roc_auc_score(test_y, rf_clf.predict(test_d))
fpr, tpr, _ = metrics.roc_curve(test_y, rf_clf.predict(test_d)); metrics.auc(fpr, tpr)
accuracy_score(test_y, rf_clf.predict(test_d))
rf_clf = RandomForestClassifier(100)
rf_clf.fit(train_d, train_y)
accuracy_score(test_y, rf_clf.predict(test_d))
roc_auc_score(test_y, rf_clf.predict(test_d))
fpr, tpr, _ = metrics.precision_recall_curve(test_y, rf_clf.predict(test_d)); metrics.auc(fpr, tpr)
rf_clf
x = rf_clf.predict(test_d)
x[:5]
?rf_clf.predict
x = rf_clf.predict_proba(test_d)
x[:5]
fpr, tpr, _ = metrics.precision_recall_curve(test_y, rf_clf.predict_proba(test_d)); metrics.auc(fpr, tpr)
fpr, tpr, _ = metrics.precision_recall_curve(test_y, rf_clf.predict_proba(test_d)[0]); metrics.auc(fpr, tpr)
fpr, tpr, _ = metrics.precision_recall_curve(test_y, rf_clf.predict_proba(test_d)[,1]); metrics.auc(fpr, tpr)
fpr, tpr, _ = metrics.precision_recall_curve(test_y, rf_clf.predict_proba(test_d)[:,0]); metrics.auc(fpr, tpr)
x = rf_clf.predict_proba(test_d)
x[1:10,0]
x.shape
x[:,0].shape
x[:,0].reshape((1, 4964)).shape
?reshape
x2 = reshape(x[:,0], x.shape[0])
x2.shape
fpr, tpr, _ = metrics.precision_recall_curve(test_y, rf_clf.predict_proba(test_d)[:,0]); metrics.auc(fpr, tpr)
fpr, tpr, _ = metrics.precision_recall_curve(test_y, rf_clf.predict_proba(test_d)[:,0]); metrics.auc(tpr, fpr)
fpr, tpr, _ = metrics.precision_recall_curve(test_y, rf_clf.predict_proba(test_d)[:,1]); metrics.auc(tpr, fpr)
precision, recall, _ = metrics.precision_recall_curve(test_y, rf_clf.predict_proba(test_d)[:,1]); metrics.auc(recall, precision)
plot(recall, precision)
fpr, tpr, _ = metrics.roc_curve(test_y, rf_clf.predict_proba(test_d)[:,1]); metrics.auc(fpr, tpr)
plot(tpr, fpr)
plot(fpr, tpr)
%history -f random_forest_vs_NN.py
