Run 1

Logistic regression:
	Accuracy: 0.857977
	PR_AUC: 0.747544
	ROC_AUC: 0.896521

Random forest (100 trees):
	Accuracy: 0.862208
	PR_AUC: 0.782321
	ROC_AUC: 0.906051

DNN classifier (10^4 training epochs):
    Accuracy: 0.87207896
    PR_AUC: 0.79137921
    ROC_AUC: 0.92056614