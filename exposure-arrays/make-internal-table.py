import json
import csv
import numpy as np
import pandas as pd
import convertCSV2JSON as c2j
from IPAParser import parsePhon
from string import capwords
from itertools import combinations
from geopy.distance import VincentyDistance
from PhonoSearchLib import LangSearchEngine

# Helper functions
join_by_comma = lambda x: ', '.join(str(el) for el in x)
to_lower      = lambda x: x.lower()
map_to_list   = lambda x, func: list(map(func, x))
set_to_string = lambda x: ' '.join(str(el) for el in x)

# Data from EURPhon
engine = LangSearchEngine('../dbase/phono_dbase.json', False)

# PHOIBLE data except those langs that are present
# in EURPhon or are taken from SPA (they overlap with UPSID)
lang_data      = pd.read_csv('../phoible-aggregated.tsv', sep='\t')
phon_data      = pd.read_csv('../phoible-phonemes.tsv', sep='\t')
eur_lang_codes = [el['code'] for el in engine.lang_dic.values()]
sample = lang_data[lang_data['Area'].isin(['Asia', 'Europe']) &
                   ~lang_data['Country'].isin(['Indonesia', 'Malaysia', 'Brunei']) &
                   ~lang_data['LanguageCode'].isin(eur_lang_codes) &
                   ~lang_data['Source'].isin(['SPA'])]

# Unification of phylum and genus names
with open('../phoible_groups_to_langs.json', 'r', encoding='utf-8') as inp:
    groups_to_fams = json.load(inp)

sample_codes = sample['LanguageCode'].unique()
sample_names = map_to_list(sample['LanguageName'].unique(), to_lower)

# Extract consonant data from PHOIBLE
phono_sample = phon_data[(phon_data['LanguageCode'].isin(sample_codes) |
                         phon_data['LanguageName'].apply(to_lower).isin(sample_names)) &
                         ~phon_data['Source'].isin(['SPA']) &
                         phon_data['Class'].isin(['consonant'])]
phono_sample = phono_sample[['LanguageCode', 'LanguageName', 'Phoneme']]

# Throw away languages with non-IPA symbols in the descriptions
lang_blacklist = set()
for i, p in enumerate(phono_sample['Phoneme']):
    try:
        parsePhon(p)
    except Exception as error:
        print(phono_sample.iloc[i,])
        lang_blacklist.add(capwords(phono_sample.iloc[i,]['LanguageName'].lower()))
lang_blacklist = list(lang_blacklist)

# Convert the data to the language-per-row format
inventories = phono_sample.groupby('LanguageName').agg(join_by_comma)
inventories['LanguageCode'] = inventories['LanguageCode'].apply(lambda x: x.split(', ')[0])
inventories[inventories.index.name] = pd.Series(inventories.index).apply(capwords)
inventories['LanguageName'] = list(map_to_list(inventories.index, capwords))
inventories = inventories[~inventories['LanguageName'].isin(lang_blacklist)]

inventories['Phylum'] = None
inventories['Genus'] = None
inventories['Latitude'] = None
inventories['Longitude'] = None

genera = []
lats   = []
lons   = []
for i, ln in enumerate(inventories['LanguageName']):
    rows = lang_data[~lang_data['Source'].isin(['SPA']) &
                     (lang_data['LanguageName'].apply(capwords) == ln)]
    # All subsets are one-row long by definition,
    # cf. the groupby statement above
    genus = rows.iloc[0,]['LanguageFamilyGenus'].strip()
    inventories['Genus'][i] = genus
    phylum = groups_to_fams[genus]
    inventories['Phylum'][i] = phylum
    try:
        lat = float(rows.iloc[0,]['Latitude'].replace(':', '.'))
        inventories['Latitude'][i] = lat
    except:
        continue
    try:
        lon = float(rows.iloc[0,]['Longitude'].replace(':', '.'))
        inventories['Longitude'][i] = lon
    except:
        continue

# Add data needed by the EURPhon parser
# and rearrange columns
inventories['Timestamp'] = None
inventories['Lang_or_dial'] = "Язык"
inventories['Source'] = "PHOIBLE"
inventories['Source_short'] = None
inventories['Vowels'] = None
inventories['Tones'] = None
inventories['Syllables'] = None
inventories['Initial_clusters'] = None
inventories['Finals'] = None
inventories['Comments'] = None
inventories['Added_by'] = "PHOIBLE"

new_col_list = ['Timestamp',
                'LanguageName',
                'LanguageCode',
                'Latitude',
                'Longitude',
                'Lang_or_dial',
                'Phylum',
                'Genus',
                'Source',
                'Source_short',
                'Phoneme',
                'Vowels',
                'Tones',
                'Syllables',
                'Initial_clusters',
                'Finals',
                'Comments',
                'Added_by'
               ]

inventories = inventories[new_col_list]

# Dump the data in the new aggregated file
# for subsequent processing
with open('../dbase/ffli-dbase.csv', 'r', encoding='utf-8') as inp:
    with open('all_data.csv', 'w', encoding='utf-8') as out:
        out.write(inp.read() + '\n')
        for i in range(inventories.shape[0]):
            out.write('\t'.join(str(el) for el in inventories.iloc[i,]) + '\n')

with open ('all_data.json', 'w', encoding='utf-8') as out:
    json.dump(c2j.convert2JSON("all_data.csv"), out, indent=4, ensure_ascii=False)

# Read the data and get the search engine going
big_engine = LangSearchEngine('all_data.json', False)

names = []
lats  = []
lons  = []
phyla = []
genera = []
inventory_sets = []
for key, val in big_engine.coord_dic.items():
    names.append(key.split('#')[0])
    lat, lon = val
    lats.append(lat)
    lons.append(lon)
    if lat == "None" or lon == "None":
        print(key)
    phyla.append(big_engine.lang_dic[key]['gen'][0])
    genera.append(big_engine.lang_dic[key]['gen'][1])
    inventory_sets.append(set(el.difference({'lateral', 'non_lateral'}) for el in big_engine.inv_dic[key] if 'consonant' in el))
coords_table = pd.DataFrame({'LangName': names, 'Latitude': lats, 'Longitude': lons,
                             'Phylum': phyla, 'Genus': genera})
coords_table = coords_table[['LangName', 'Latitude', 'Longitude', 'Phylum', 'Genus']]
coords_table.to_csv('all_data_coords.csv', index=False)

all_phonemes = sorted(set.union(*inventory_sets))

lats_float = list(map(np.float, lats))
lons_float = list(map(np.float, lons))

# Restrict data by phonemes that appear no less than 20 times
count_dic = {}
for el in inventory_sets:
    for p in el:
        if p in count_dic:
            count_dic[p] += 1
        else:
            count_dic[p] = 1

all_phonemes = [el for el in all_phonemes if count_dic[el] >= 20]

def row_creation_factory(all_phonemes):
    idx_dic = {}
    for i, el in enumerate(all_phonemes):
        idx_dic[el] = i
    def create_row(s):
        arr = [0 for i in range(len(all_phonemes))]
        for el in s:
            if el not in all_phonemes:
                continue
            arr[idx_dic[el]] = 1
        return arr
    return create_row

make_row = row_creation_factory(all_phonemes)
        
with open('internal_data.csv', 'w') as out:
    w = csv.writer(out)

    header_row = [
        'lang',
        'feature_name',
        'phylum',
        'genus'
    ]
    header_row.extend([set_to_string(el) for el in all_phonemes])
    header_row.append('feature_present')

    w.writerow(header_row)

    for l in range(len(names)):
        
        print(names[l], lats_float[l], lons_float[l])

        for p in all_phonemes:
            r = [
                names[l],
                '',
                phyla[l],
                genera[l]
            ]
            punctured_set = set(el for el in inventory_sets[l])
            punctured_set.discard(p)
            r.extend(make_row(punctured_set))
            r.append(0)
            r[1] = set_to_string(p)
            if p in inventory_sets[l]:
                r[-1] = 1
            else:
                r[-1] = 0
            w.writerow(r)


