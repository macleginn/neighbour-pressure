import json
import csv
import numpy as np
import pandas as pd
import networkx as nx
import convertCSV2JSON as c2j
from IPAParser import parsePhon
from string import capwords
from itertools import combinations
from geopy.distance import VincentyDistance
from PhonoSearchLib import LangSearchEngine

# Helper functions
join_by_comma = lambda x: ', '.join(str(el) for el in x)
to_lower      = lambda x: x.lower()
map_to_list   = lambda x, func: list(map(func, x))
set_to_string = lambda x: ' '.join(str(el) for el in x)

# Data from EURPhon
engine = LangSearchEngine('../dbase/phono_dbase.json', False)

# PHOIBLE data except those langs that are present
# in EURPhon or are taken from SPA (they overlap with UPSID)
lang_data      = pd.read_csv('../phoible-aggregated.tsv', sep='\t')
phon_data      = pd.read_csv('../phoible-phonemes.tsv', sep='\t')
eur_lang_codes = [el['code'] for el in engine.lang_dic.values()]
sample = lang_data[lang_data['Area'].isin(['Asia', 'Europe']) &
                   ~lang_data['Country'].isin(['Indonesia', 'Malaysia', 'Brunei']) &
                   ~lang_data['LanguageCode'].isin(eur_lang_codes) &
                   ~lang_data['Source'].isin(['SPA'])]

# Unification of phylum and genus names
with open('../phoible_groups_to_langs.json', 'r', encoding='utf-8') as inp:
    groups_to_fams = json.load(inp)

sample_codes = sample['LanguageCode'].unique()
sample_names = map_to_list(sample['LanguageName'].unique(), to_lower)

# Extract consonant data from PHOIBLE
phono_sample = phon_data[(phon_data['LanguageCode'].isin(sample_codes) |
                         phon_data['LanguageName'].apply(to_lower).isin(sample_names)) &
                         ~phon_data['Source'].isin(['SPA']) &
                         phon_data['Class'].isin(['consonant'])]
phono_sample = phono_sample[['LanguageCode', 'LanguageName', 'Phoneme']]

# Throw away languages with non-IPA symbols in the descriptions
lang_blacklist = set()
for i, p in enumerate(phono_sample['Phoneme']):
    try:
        parsePhon(p)
    except Exception as error:
        print(phono_sample.iloc[i,])
        lang_blacklist.add(capwords(phono_sample.iloc[i,]['LanguageName'].lower()))
lang_blacklist = list(lang_blacklist)

# Convert the data to the language-per-row format
inventories = phono_sample.groupby('LanguageName').agg(join_by_comma)
inventories['LanguageCode'] = inventories['LanguageCode'].apply(lambda x: x.split(', ')[0])
inventories[inventories.index.name] = pd.Series(inventories.index).apply(capwords)
inventories['LanguageName'] = list(map_to_list(inventories.index, capwords))
inventories = inventories[~inventories['LanguageName'].isin(lang_blacklist)]

inventories['Phylum'] = None
inventories['Genus'] = None
inventories['Latitude'] = None
inventories['Longitude'] = None

genera = []
lats   = []
lons   = []
for i, ln in enumerate(inventories['LanguageName']):
    rows = lang_data[~lang_data['Source'].isin(['SPA']) &
                     (lang_data['LanguageName'].apply(capwords) == ln)]
    # All subsets are one-row long by definition,
    # cf. the groupby statement above
    genus = rows.iloc[0,]['LanguageFamilyGenus'].strip()
    inventories['Genus'][i] = genus
    phylum = groups_to_fams[genus]
    inventories['Phylum'][i] = phylum
    try:
        lat = float(rows.iloc[0,]['Latitude'].replace(':', '.'))
        inventories['Latitude'][i] = lat
    except:
        continue
    try:
        lon = float(rows.iloc[0,]['Longitude'].replace(':', '.'))
        inventories['Longitude'][i] = lon
    except:
        continue

# Add data needed by the EURPhon parser
# and rearrange columns
inventories['Timestamp'] = None
inventories['Lang_or_dial'] = "Язык"
inventories['Source'] = "PHOIBLE"
inventories['Source_short'] = None
inventories['Vowels'] = None
inventories['Tones'] = None
inventories['Syllables'] = None
inventories['Initial_clusters'] = None
inventories['Finals'] = None
inventories['Comments'] = None
inventories['Added_by'] = "PHOIBLE"

new_col_list = ['Timestamp',
                'LanguageName',
                'LanguageCode',
                'Latitude',
                'Longitude',
                'Lang_or_dial',
                'Phylum',
                'Genus',
                'Source',
                'Source_short',
                'Phoneme',
                'Vowels',
                'Tones',
                'Syllables',
                'Initial_clusters',
                'Finals',
                'Comments',
                'Added_by'
               ]

inventories = inventories[new_col_list]

# Dump the data in the new aggregated file
# for subsequent processing
with open('../dbase/ffli-dbase.csv', 'r', encoding='utf-8') as inp:
    with open('all_data.csv', 'w', encoding='utf-8') as out:
        out.write(inp.read() + '\n')
        for i in range(inventories.shape[0]):
            out.write('\t'.join(str(el) for el in inventories.iloc[i,]) + '\n')

with open ('all_data.json', 'w', encoding='utf-8') as out:
    json.dump(c2j.convert2JSON("all_data.csv"), out, indent=4, ensure_ascii=False)

# Read the data and get the search engine going
big_engine = LangSearchEngine('all_data.json', False)

names = []
lats  = []
lons  = []
phyla = []
genera = []
inventory_sets = []
for key, val in big_engine.coord_dic.items():
    names.append(key.split('#')[0])
    lat, lon = val
    lats.append(lat)
    lons.append(lon)
    if lat == "None" or lon == "None":
        print(key)
    phyla.append(big_engine.lang_dic[key]['gen'][0])
    genera.append(big_engine.lang_dic[key]['gen'][1])
    inventory_sets.append(set(el.difference({'lateral', 'non_lateral'}) for el in big_engine.inv_dic[key] if 'consonant' in el))
coords_table = pd.DataFrame({'LangName': names, 'Latitude': lats, 'Longitude': lons,
                             'Phylum': phyla, 'Genus': genera})
coords_table = coords_table[['LangName', 'Latitude', 'Longitude', 'Phylum', 'Genus']]
coords_table.to_csv('all_data_coords.csv', index=False)

# Delaunay triangulation on a sphere = 3d convex hull

from scipy.spatial import ConvexHull

points_cartesian = np.ndarray((0, 3))
for i in range(len(lats)):
    lat = np.deg2rad(np.float(lats[i]))
    lon = np.deg2rad(np.float(lons[i]))
    x = np.cos(lat) * np.cos(lon)
    y = np.cos(lat) * np.sin(lon)
    z = np.sin(lat)
    points_cartesian = np.vstack((points_cartesian, (x, y, z)))

qhull = ConvexHull(points_cartesian)

# Graph analysis
lang_graph = nx.Graph()

lats_float = list(map(np.float, lats))
lons_float = list(map(np.float, lons))

for i in range(qhull.simplices.shape[0]):
    for pair in combinations(qhull.simplices[i], 2):
        p = pair[0]; q = pair[1]
        lat1, lon1 = lats_float[p], lons_float[p]
        lat2, lon2 = lats_float[q], lons_float[q]
        d = VincentyDistance((lat1, lon1),
                             (lat2, lon2)).kilometers
        if d < 1000:
            lang_graph.add_edge(p, q)

def row_creation_factory(all_phonemes):
    idx_dic = {}
    for i, el in enumerate(all_phonemes):
        idx_dic[el] = i
    def create_row(s):
        arr = [0 for i in range(len(all_phonemes))]
        for el in s:
            if el not in all_phonemes:
                continue
            arr[idx_dic[el]] = 1
        return arr
    return create_row

all_phonemes = sorted(set.union(*inventory_sets))

# Restrict data by phonemes that appear no less than 20 times
count_dic = {}
for el in inventory_sets:
    for p in el:
        if p in count_dic:
            count_dic[p] += 1
        else:
            count_dic[p] = 1
all_phonemes = [el for el in all_phonemes if count_dic[el] >= 20]

make_row = row_creation_factory(all_phonemes)

# Calculate densities for the segments

def pseudo_exponential_density(accum, dist, k):
    return accum + 1.0/(k**dist)

exp_dens_base_2 = lambda accum, dist: pseudo_exponential_density(accum, dist, 2)

names_dict = { name : i for i, name in enumerate(names) }

# Get a list of points for a given feature
def get_point_list(query_string, search_engine, points_dict):
    lang_list = big_engine.features_query(query_string)
    temp = []
    for el in lang_list:
            temp.append(el)
    return [ names_dict[l.split('#')[0]] for l in temp ]

# Compute a density distribution for a given feature of interest in the neighbour graph.
# Feature of interest is represented by a list of points possessing this feature.
def compute_density_distribution(lang_graph, points_list, accumulator_fun):
    distr_dict = {}
    for node in lang_graph.nodes():
        statistic = 0
        for p in points_list:
            # The points not possessing the feature add 0 to the metric.
            # Some points could be missing from the lang_graph
            # due to their extreme geographic isolation.
            if p not in lang_graph: continue
            # Presence of feature of interest in the node, for which
            # the metric is computed, is ignored.
            if node != p:
                try:
                    dist = nx.shortest_path_length(lang_graph, node, p)
                    statistic = accumulator_fun(statistic, dist)
                except nx.NetworkXNoPath:
                    # The point belongs to a different component.
                    continue
        distr_dict[node] = statistic
    return distr_dict

shortest_paths_dicts = {}
for n in lang_graph.nodes():
    shortest_paths_dicts[n] = nx.single_source_shortest_path_length(lang_graph, n)

# Index density of every segment for every language to add
# to the final dataframe later
density_dic = {}
print('Computing densities...')
for c in all_phonemes:
    c_str = set_to_string(c)
    print(c_str)
    point_list = set(get_point_list(c_str,
                                    big_engine,
                                    names_dict))
    densities = compute_density_distribution(lang_graph,
                                             point_list,
                                             exp_dens_base_2)
    for key in densities:
        density_dic[(names[key], c)] = densities[key]
print()
        
with open('external_internal_data.csv', 'w') as out:
    w = csv.writer(out)

    header_row = [
        'lang',
        'feature_name',
        'density',
        'phylum',
        'genus'
    ]
    # External data
    header_row.extend([set_to_string(el) for el in all_phonemes])
    # Internal data
    header_row.extend([set_to_string(el) for el in all_phonemes])
    header_row.append('feature_present')

    w.writerow(header_row)

    for l in lang_graph:
        r = [
            names[l],
            '',
            0,
            phyla[l],
            genera[l]
        ]

        print(names[l], lats_float[l], lons_float[l])

        exposure_set = set()
        for n in lang_graph.neighbors(l):
            exposure_set = set.union(exposure_set, inventory_sets[n])
        r.extend(make_row(exposure_set))
        punctured_set = set(el for el in inventory_sets[l])
        punctured_set.discard(p)
        r.extend(make_row(punctured_set))
        r.append(0)
        for p in all_phonemes:
            r[1] = set_to_string(p)
            r[2] = density_dic[(names[l], p)]
            if p in inventory_sets[l]:
                r[-1] = 1
            else:
                r[-1] = 0
            w.writerow(r)


