library(randomForest)
library(pROC)

d = read.csv('external-data-genus.csv', h = T)

d_clean = d[ -which( d$phylum %in% c('Isolate', 'Creoles and Pidgins')), ]
d_mm = as.data.frame(model.matrix(feature_present ~ . - lang, data = d_clean))
d_mm = d_mm[,-1]
d_mm$response = factor(d_clean$feature_present)
for (i in 1:(ncol(d_mm)-1)) {
    colnames(d_mm)[i] = paste0(substr(colnames(d_mm)[i], 1, 4), toString(i))
}

chunk.size = nrow(d_mm) %/% 10
chunk.indices = list()
all.indices = seq(1, nrow(d_mm))
for (i in 1:9) {
    chunk.indices[[i]] = sample(all.indices, chunk.size)
    all.indices = all.indices[ -which(all.indices %in% chunk.indices[[i]]) ]
}
chunk.indices[[10]] = all.indices

accuracies = c()
rocaucs    = c()

for (i in 1:10) {
    print(i)
    d_train = d_mm[ -chunk.indices[[i]], ]
    d_test  = d_mm[ chunk.indices[[i]], ]
    rf.fit  = randomForest(response ~ ., data = d_train, ntree = 100)
    pred.class = as.numeric(predict(rf.fit, newdata = d_test)) - 1
    pred.probs = predict(rf.fit, newdata = d_test, type = 'prob')[,2]
    truth = as.numeric(d_test$response) - 1
    
    accuracy = 1 - sum(abs(pred.class - truth)) / length(truth)
    accuracies[i] = accuracy
    print(accuracy)

    rocauc = auc(roc(truth, pred.probs))
    rocaucs[i] = rocauc
    print(rocauc)
}
