import pandas as pd
import numpy as np
# import tensorflow as tf
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier

dat = pd.read_csv('external_internal_data.csv').fillna("")

# Separate training and test data indices
row_ns = list(range(dat.shape[0]))
test_ids = np.random.choice(row_ns,
                            len(row_ns)//10,
                            replace = False)
train_ids = [el for el in row_ns if el not in test_ids]

# Train baseline logistic regression
# using only phoneme frequencies
lr_simple_data_full = pd.get_dummies(dat[['feature_name']])
lr_simple_labels_full = dat['feature_present']

lr_simple_train_X = lr_simple_data_full.loc[train_ids]
lr_simple_train_y = lr_simple_labels_full.loc[train_ids]

lr_simple_test_X = lr_simple_data_full.loc[test_ids]
lr_simple_test_y = lr_simple_labels_full.loc[test_ids]

lr_simple_clf = LogisticRegression()
lr_simple_clf.fit(lr_simple_train_X, lr_simple_train_y)
lr_simple_preds_proba = lr_simple_clf.predict_proba(lr_simple_test_X)[:,1]

# Reporting accuracy, ROC-AUC, and PR-AUC
lr_simple_clf_accuracy = metrics.accuracy_score(
    lr_simple_test_y,
    lr_simple_clf.predict(lr_simple_test_X)
)

precision, recall, _ = metrics.precision_recall_curve(
    lr_simple_test_y,
    lr_simple_preds_proba
)
lr_simple_clf_PR_AUC = metrics.auc(recall, precision)

lr_simple_clf_ROC_AUC = metrics.roc_auc_score(
    lr_simple_test_y,
    lr_simple_preds_proba
)


# Train logistic regression using
# feature_name, phylum, and genus
logreg_data_full   = dat[['feature_name',
                          'phylum',
                          'genus']]
logreg_data_full = pd.get_dummies(logreg_data_full,
                                  columns = ['feature_name',
                                             'phylum',
                                             'genus'])
logreg_labels_full = dat['feature_present']

logreg_train_X     = logreg_data_full.loc[train_ids]
logreg_train_y     = logreg_labels_full[train_ids]
logreg_test_X      = logreg_data_full.loc[test_ids]
logreg_test_y      = logreg_labels_full[test_ids]

lr_clf = LogisticRegression()
lr_clf.fit(logreg_train_X, logreg_train_y)
lr_preds_proba = lr_clf.predict_proba(logreg_test_X)[:,1]

# Reporting accuracy, ROC-AUC, and PR-AUC
lr_clf_accuracy = metrics.accuracy_score(
    logreg_test_y,
    lr_clf.predict(logreg_test_X)
)

precision, recall, _ = metrics.precision_recall_curve(
    logreg_test_y,
    lr_preds_proba
)
lr_clf_PR_AUC = metrics.auc(recall, precision)

lr_clf_ROC_AUC = metrics.roc_auc_score(
    logreg_test_y,
    lr_preds_proba
)

# Train a 100-trees random-forest classifier using punctured
# inventories, phylum, and genus
rf_data_full = dat.drop(['lang', 'feature_present', 'density'], axis = 1)
rf_data_full = pd.get_dummies(rf_data_full,
                              columns = [
                                  'feature_name',
                                  'phylum',
                                  'genus'
                              ])
rf_train_X = rf_data_full.loc[train_ids]
rf_train_y = logreg_train_y
rf_test_X = rf_data_full.loc[test_ids]
rf_test_y = logreg_test_y

rf_clf = RandomForestClassifier(100)
rf_clf.fit(rf_train_X, rf_train_y)
rf_preds_proba = rf_clf.predict_proba(rf_test_X)[:,1]

rf_clf_accuracy = metrics.accuracy_score(
    rf_test_y,
    rf_clf.predict(rf_test_X)
)

precision, recall, _ = metrics.precision_recall_curve(
    rf_test_y,
    rf_preds_proba
)
rf_clf_PR_AUC = metrics.auc(recall, precision)

rf_clf_ROC_AUC = metrics.roc_auc_score(
    rf_test_y,
    rf_preds_proba
)

# Train a deep neural classifier using the same features

# tf_train_X = rf_train_X.as_matrix()
# tf_train_y = rf_train_y.as_matrix()

# tf_test_X = rf_test_X.as_matrix()
# tf_test_y = rf_test_y.as_matrix()

# train_input_fn = tf.estimator.inputs.numpy_input_fn(
#     x = { 'x': tf_train_X },
#     y = tf_train_y,
#     num_epochs = None,
#     shuffle = True
# )

# feature_columns = tf.contrib.learn.infer_real_valued_columns_from_input_fn(train_input_fn)

# tf_clf = tf.estimator.DNNClassifier(feature_columns = feature_columns,
#                                     hidden_units=[183,91,45,22,11,5])
# tf_clf.train(input_fn=train_input_fn,
#              steps = 10**4)

# test_input_fn = tf.estimator.inputs.numpy_input_fn(
#     x = { 'x': tf_test_X },
#     y = tf_test_y,
#     num_epochs = 1,
#     shuffle = False
# )

print("Simplistic logistic regression:\n\tAccuracy: %f\n\tPR_AUC: %f\n\tROC_AUC: %f" % (
    lr_simple_clf_accuracy,
    lr_simple_clf_PR_AUC,
    lr_simple_clf_ROC_AUC
))
print()

print("Logistic regression:\n\tAccuracy: %f\n\tPR_AUC: %f\n\tROC_AUC: %f" % (
    lr_clf_accuracy,
    lr_clf_PR_AUC,
    lr_clf_ROC_AUC
))
print()

print("Random forest (100 trees):\n\tAccuracy: %f\n\tPR_AUC: %f\n\tROC_AUC: %f" % (
    rf_clf_accuracy,
    rf_clf_PR_AUC,
    rf_clf_ROC_AUC
))
print()

# print("DNN classifier:")
# print(tf_clf.evaluate(input_fn=test_input_fn))
