import pandas as pd

d = pd.read_csv('internal_data.csv')

memo = {}                                             
d_copy = d.copy()
blacklist = set()
for i in range(d.shape[0]):
    print(i)
    row = d.loc[i,]
    lang = row['lang']
    phylum = row['phylum']
    if phylum in {'Isolate', 'Creoles and Pidgins'}:
        blacklist.add((lang, phylum))
        continue
    if lang in memo:
        lang_vals = memo[lang]
    else:
        print('Creating phylum set for %s' % lang)
        lang_df = d.loc[ (d['phylum'] == phylum) & (d['lang'] != lang), ]
        lang_vals = [0 for idx in range(d.shape[1]-1)]
        for j in range(4, d.shape[1]-1):
            if (lang_df.iloc[:,j] != 0).any():
                lang_vals[j] = 1
        memo[lang] = lang_vals
    for j in range(4, d.shape[1]-1):
        d_copy.iloc[i,j] = lang_vals[j]

d_copy.to_csv('external-data-phylum.csv', index = False)
