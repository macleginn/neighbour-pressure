Genus sets:

[1] 1
[1] 0.8672688
Area under the curve: 0.8982
[1] 2
[1] 0.8698213
Area under the curve: 0.8973
[1] 3
[1] 0.8776752
Area under the curve: 0.8993
[1] 4
[1] 0.8802278
Area under the curve: 0.8943
[1] 5
[1] 0.8629491
Area under the curve: 0.896
[1] 6
[1] 0.8739446
Area under the curve: 0.9038
[1] 7
[1] 0.8698213
Area under the curve: 0.8988
[1] 8
[1] 0.8674651
Area under the curve: 0.889
[1] 9
[1] 0.8655017
Area under the curve: 0.8962
[1] 10
[1] 0.8736016
Area under the curve: 0.8944


Phylum sets:

[1] 1
[1] 0.8653053
Area under the curve: 0.8892
[1] 2
[1] 0.8649126
Area under the curve: 0.8876
[1] 3
[1] 0.8602003
Area under the curve: 0.8796
[1] 4
[1] 0.8639309
Area under the curve: 0.8847
[1] 5
[1] 0.8588258
Area under the curve: 0.8845
[1] 6
[1] 0.865698
Area under the curve: 0.8881
[1] 7
[1] 0.8625565
Area under the curve: 0.8743
[1] 8
[1] 0.8623601
Area under the curve: 0.8872
[1] 9
[1] 0.865698
Area under the curve: 0.8858
[1] 10
[1] 0.8635918
Area under the curve: 0.8894
