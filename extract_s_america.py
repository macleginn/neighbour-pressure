import json
import pandas as pd
import numpy as np
import os.path
import convertCSV2JSON as c2j
from PhonoSearchLib import LangSearchEngine
from scipy.spatial import Delaunay
from string import capwords
from IPAParser import parseCons, parsePhon

join_by_comma = lambda x: ', '.join(str(el) for el in x)
to_lower = lambda x: x.lower()
map_to_list = lambda x, func: list(map(func, x))

# Phyla and genera
lang_data = pd.read_csv('phoible-aggregated.tsv', sep='\t')

# PHOIBLE from SAPHON
phon_data = pd.read_csv('phoible-saphon.tsv', sep='\t')
phono_sample = phon_data[(phon_data['Source'] == 'SAPHON') &
                         (phon_data['Class'] == 'consonant')]

# Throw away languages with non-IPA symbols in the descriptions
lang_blacklist = set()
for i, p in enumerate(phono_sample['Phoneme']):
    try:
        parsePhon(p)
    except Exception as error:
        print(phono_sample.iloc[i,])
        lang_blacklist.add(capwords(phono_sample.iloc[i,]['LanguageName'].lower()))
lang_blacklist = list(lang_blacklist)

# Convert the data to the language-per-row format
inventories = phono_sample.groupby('LanguageName').agg(join_by_comma)
inventories['LanguageCode'] = inventories['LanguageCode'].apply(lambda x: x.split(', ')[0])
inventories[inventories.index.name] = pd.Series(inventories.index).apply(capwords)
inventories['LanguageName'] = list(map_to_list(inventories.index, capwords))
inventories = inventories[~inventories['LanguageName'].isin(lang_blacklist)]

inventories['Phylum'] = None
inventories['Genus'] = None
inventories['Latitude'] = None
inventories['Longitude'] = None

genera = []
lats   = []
lons   = []
for i, ln in enumerate(inventories['LanguageName']):
    rows = lang_data[lang_data['LanguageName'].apply(capwords) == ln]
    # All subsets are one-row long by definition,
    # cf. the groupby statement above
    genus = rows.iloc[0,]['LanguageFamilyGenus'].strip()
    phylum = rows.iloc[0,]['LanguageFamilyRoot'].strip()
    inventories['Genus'][i] = genus
    inventories['Phylum'][i] = phylum
    try:
        lat = float(rows.iloc[0,]['Latitude'].replace(':', '.'))
        inventories['Latitude'][i] = lat
    except:
        continue
    try:
        lon = float(rows.iloc[0,]['Longitude'].replace(':', '.'))
        inventories['Longitude'][i] = lon
    except:
        continue

# Add data needed by the EURPhon parser
# and rearrange columns
inventories['Timestamp'] = None
inventories['Lang_or_dial'] = "Язык"
inventories['Source'] = "SAPHON via PHOIBLE"
inventories['Source_short'] = None
inventories['Vowels'] = None
inventories['Tones'] = None
inventories['Syllables'] = None
inventories['Initial_clusters'] = None
inventories['Finals'] = None
inventories['Comments'] = None
inventories['Added_by'] = "SAPHON via PHOIBLE"

new_col_list = ['Timestamp',
                'LanguageName',
                'LanguageCode',
                'Latitude',
                'Longitude',
                'Lang_or_dial',
                'Phylum',
                'Genus',
                'Source',
                'Source_short',
                'Phoneme',
                'Vowels',
                'Tones',
                'Syllables',
                'Initial_clusters',
                'Finals',
                'Comments',
                'Added_by'
               ]

inventories = inventories[new_col_list]