library(ggmap)
# setwd('/home/macleginn/Documents/neighbour-pressure')

d = read.csv('exposure-arrays/bin_data.csv', h = T)
d.coords = read.csv('exposure-arrays/all_data_coords.csv', h = T)
d.map = merge(d[,c('lang',
				   'feature_name',
				   'feature_present',
				   'phylum',
				   'genus')],
	          d.coords[,c('LangName',
	          			  'Latitude',
	          			  'Longitude')],
	          by.x='lang', by.y = 'LangName')

p.vals = read.csv('p-vals.csv', h = T)

pop.segs = p.vals$phoneme[7:12]
d.pop = droplevels(d.map[(d.map$feature_name %in% pop.segs) & (d.map$feature_present == 1),])
pdf('top-7-12.pdf', width = 10, height = 16)
ggmap(map) + geom_point(data = d.pop, aes(x = Longitude, y = Latitude, color = phylum)) +
	facet_wrap(~ feature_name, ncol = 2)
# qmplot(Longitude, Latitude, data = d.pop, color = phylum, map = map) +
# 	facet_wrap(~ feature_name, ncol = 2)
dev.off()

eur = c(left = -28,
		bottom = 0,
		right = 170,
		top = 78)
map = get_map(eur, maptype = 'toner-lite')


# unpop.segs = p.vals$phoneme[(nrow(p.vals)-5):nrow(p.vals)]
# d.unpop = droplevels(d.map[(d.map$feature_name %in% unpop.segs) & (d.map$feature_present == 1),])
# pdf('tail-6.pdf', width = 10, height = 16)
# ggmap(map) + geom_point(data = d.unpop, aes(x = Longitude, y = Latitude, color = phylum)) +
# 	facet_wrap(~ feature_name, ncol = 2)
# dev.off()