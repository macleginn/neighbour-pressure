0.4166667 labial-velar approximant voiced
0.4884259 alveolar plosive voiceless
0.4978261 alveolar plosive voiced
0.5 alveolar approximant voiced palatalised
0.5 alveolar lateral_approximant voiced palatalised
0.5044444 alveolar trill voiced
0.5121951 bilabial nasal voiced
0.5121951 alveolo-palatal fricative voiced
0.5227273 velar fricative voiced
0.5320513 alveolo-palatal affricate voiced
0.5357143 alveolar nasal voiced
0.546798 velar fricative voiceless
0.5473684 uvular plosive voiceless aspirated
0.5609756 palatal affricate voiceless
0.5803167 glottal plosive
0.581448 alveolar approximant voiced
0.581448 alveolar lateral_approximant voiced
0.5878378 alveolo-palatal affricate voiceless
0.5961538 alveolo-palatal fricative voiceless

0.6153846 palatal approximant voiced
0.6219512 labiodental approximant voiceless
0.6238095 uvular fricative voiceless
0.625 alveolar tap voiced
0.6428571 alveolar nasal voiced palatalised
0.65 interdental fricative voiced
0.6626984 pharyngeal fricative voiceless
0.6666667 alveolar fricative voiceless
0.6789474 palatal plosive voiced
0.6794872 alveolar plosive voiceless aspirated
0.6829268 velar plosive voiceless
0.6916667 postalveolar affricate voiceless aspirated
0.695122 palatal fricative voiced
0.6987179 interdental fricative voiceless

0.7021739 glottal fricative voiceless
0.7073864 labiodental fricative voiced
0.7106481 uvular plosive voiceless
0.7128205 palatal nasal voiced
0.7142857 palatal affricate voiced
0.7166667 retroflex affricate voiceless aspirated
0.7333333 uvular plosive voiced
0.7421053 bilabial plosive voiceless
0.7556818 bilabial plosive voiced
0.7613636 velar plosive voiced
0.7619048 uvular fricative voiced
0.7647059 velar plosive voiceless aspirated
0.7714932 postalveolar affricate voiced
0.7743902 alveolo-palatal affricate voiceless aspirated
0.7835498 alveolar fricative voiced
0.7840909 velar plosive voiceless glottalised
0.7882883 palatal plosive voiceless
0.7900433 velar nasal voiced

0.8039216 bilabial plosive voiceless glottalised
0.8105263 alveolar lateral_fricative voiceless
0.8157895 labiodental fricative voiceless
0.8266667 postalveolar affricate voiceless
0.8333333 palatal fricative voiceless
0.8404762 bilabial plosive voiceless aspirated
0.86 postalveolar fricative voiceless
0.8658537 retroflex fricative voiceless
0.8671171 bilabial fricative voiced
0.8782051 alveolar affricate voiceless aspirated
0.8809524 retroflex nasal voiced
0.8815789 palatal lateral_approximant voiced
0.8857143 alveolar affricate voiceless
0.8902439 retroflex affricate voiced
0.9 retroflex affricate voiceless
0.902439 retroflex plosive voiceless aspirated
0.9025641 postalveolar fricative voiced
0.9047619 alveolar trill voiceless
0.9047619 palatal affricate voiceless aspirated
0.9047619 velar plosive voiceless palatalised
0.9090909 alveolar affricate voiced
0.9166667 velar plosive voiceless labialised
0.925 palatal plosive voiceless aspirated
0.9268293 retroflex fricative voiced
0.9512195 retroflex plosive voiceless
0.9634146 alveolar approximant voiceless
0.9634146 alveolar lateral_approximant voiceless
0.9634146 retroflex plosive voiced
0.9761905 palatal nasal voiceless
0.9761905 velar nasal voiceless
1 bilabial plosive voiced aspirated
1 bilabial nasal voiceless
1 postalveolar affricate voiceless glottalised
1 velar plosive voiced aspirated