import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation

def remove_ds(p):
    for el in [
        '\u0353',
        '\u032a',
        '\u0329',
        '\u0349',
        '\u032c',
        '\u0320',
        '\u033b',
        '\u031f',
        '\u0348',
        '\u033a',
        '\u031a',
        '\u032f',
        '\u0347'
    ]:
        p = p.replace(el, '')
    return p

def print_top_words(model, feature_names, n_top_words):
    for topic_idx, topic in enumerate(model.components_):
        message = "Topic #%d: " % topic_idx
        message += " ".join([feature_names[i]
                             for i in topic.argsort()[:-n_top_words - 1:-1]])
        print(message)
    print()

d = pd.read_csv('phoible-phonemes.tsv', sep = '\t')
d = d.loc[d['Class'] == 'consonant']

d_dic = {
    'code': [el for el in d['LanguageCode']],
    'phoneme': [remove_ds(el) for el in d['Phoneme']]
}

d2 = pd.DataFrame(d_dic)
dtable = d2.groupby('code').agg({'phoneme': lambda x: ' '.join(x)})

tf_vectorizer = CountVectorizer()
tf = tf_vectorizer.fit_transform(d2['phoneme'])

lda = LatentDirichletAllocation(
    learning_method='batch',
    random_state=0
    )
lda.fit(tf)
print("\nTopics in LDA model:")
tf_feature_names = tf_vectorizer.get_feature_names()
print_top_words(lda, tf_feature_names, 20)