                                     alveolar nasal voiced 
                                               -0.04763551 
                              alveolar fricative voiceless 
                                               -0.01641171 
                                palatal approximant voiced 
                                                0.03330540 
                       alveolar lateral_approximant voiced 
                                                0.03494874 
                               alveolar approximant voiced 
                                                0.03872672 
                                   alveolar plosive voiced 
                                                0.04546592 
                                alveolar plosive voiceless 
                                                0.06367740 
                           labial-velar approximant voiced 
                                                0.09167374 
                                   bilabial plosive voiced 
                                                0.16626866 
                                     alveolar trill voiced 
                                                0.16845016 
                                      velar plosive voiced 
                                                0.21733136 
                               glottal fricative voiceless 
                                                0.23495760 
                                      palatal nasal voiced 
                                                0.25628371 
                                           glottal plosive 
                                                0.26909709 
                                        velar nasal voiced 
                                                0.29718273 
                      alveolar plosive voiceless aspirated 
                                                0.30581416 
                                       alveolar tap voiced 
                                                0.31026280 
                              alveolar affricate voiceless 
                                                0.31836150 
                         velar plosive voiceless aspirated 
                                                0.32026274 
                                   velar plosive voiceless 
                                                0.32247140 
                                 alveolar fricative voiced 
                                                0.33723286 
                      bilabial plosive voiceless aspirated 
                                                0.35162911 
                                     bilabial nasal voiced 
                                                0.38836304 
                          postalveolar affricate voiceless 
                                                0.39745256 
                             postalveolar affricate voiced 
                                                0.41327927 
                              labiodental fricative voiced 
                                                0.41676004 
                                 alveolar affricate voiced 
                                                0.41842719 
                          postalveolar fricative voiceless 
                                                0.42938456 
                                 velar fricative voiceless 
                                                0.43213614 
                                 palatal plosive voiceless 
                                                0.45690728 
                           labiodental fricative voiceless 
                                                0.48954115 
                    alveolar affricate voiceless aspirated 
                                                0.49528446 
                             postalveolar fricative voiced 
                                                0.51800605 
                       alveolo-palatal affricate voiceless 
                                                0.53109142 
                                bilabial plosive voiceless 
                                                0.53759957 
                                  retroflex plosive voiced 
                                                0.56832900 
                                    palatal plosive voiced 
                                                0.56905242 
                               retroflex plosive voiceless 
                                                0.57500604 
                postalveolar affricate voiceless aspirated 
                                                0.61182506 
                            alveolar approximant voiceless 
                                                0.61405570 
                    alveolar lateral_approximant voiceless 
                                                0.61405570 
                                    velar fricative voiced 
                                                0.62229695 
                          alveolo-palatal affricate voiced 
                                                0.63899812 
                     retroflex plosive voiceless aspirated 
                                                0.63953555 
                                uvular fricative voiceless 
                                                0.64523445 
                                  uvular plosive voiceless 
                                                0.67761398 
                       alveolo-palatal fricative voiceless 
                                                0.68403540 
                                   uvular fricative voiced 
                                                0.68564428 
                                  glottal fricative voiced 
                                                0.69304237 
                                 bilabial fricative voiced 
                                                0.69651260 
                               palatal fricative voiceless 
                                                0.70293127 
             alveolo-palatal affricate voiceless aspirated 
                                                0.72434689 
                                      retroflex tap voiced 
                                                0.72727606 
                              interdental fricative voiced 
                                                0.76142319 
                                    retroflex nasal voiced 
                                                0.76467253 
                                  bilabial nasal voiceless 
                                                0.78670329 
                          alveolo-palatal fricative voiced 
                                                0.84810084 
                           interdental fricative voiceless 
                                                0.85710748 
                                  alveolar nasal voiceless 
                                                0.86709436 
                            velar plosive voiced aspirated 
                                                0.88639896 
                       palatal plosive voiceless aspirated 
                                                0.90039377 
                              bilabial fricative voiceless 
                                                0.90553959 
                              retroflex approximant voiced 
                                                0.91649965 
                                  alveolar trill voiceless 
                                                0.91866977 
                                  palatal affricate voiced 
                                                0.92018834 
                             retroflex fricative voiceless 
                                                0.92086558 
             velar plosive voiceless aspirated palatalised 
                                                0.93741531 
                         labiodental approximant voiceless 
                                                0.94123754 
                               palatal affricate voiceless 
                                                0.94218855 
                                     velar nasal voiceless 
                                                0.94285191 
                        uvular plosive voiceless aspirated 
                                                0.95834317 
                            pharyngeal fricative voiceless 
                                                0.96370696 
                      retroflex lateral_approximant voiced 
                                                0.96388229 
                                     uvular plosive voiced 
                                                0.96908185 
                       velar plosive voiceless palatalised 
                                                0.97183607 
                   retroflex affricate voiceless aspirated 
                                                0.97646305 
                             palatal approximant voiceless 
                                                0.98328934 
                      alveolar lateral_fricative voiceless 
                                                0.98477615 
                         alveolar lateral_fricative voiced 
                                                0.98872638 
                   alveolar approximant voiced palatalised 
                                                0.99583277 
           alveolar lateral_approximant voiced palatalised 
                                                0.99583277 
                         alveolar nasal voiced palatalised 
                                                0.99698910 
                    bilabial plosive voiceless palatalised 
                                                1.00755727 
                             retroflex affricate voiceless 
                                                1.02113532 
                         bilabial plosive voiced aspirated 
                                                1.02377305 
                    alveolar plosive voiceless palatalised 
                                                1.02548156 
                    alveolar fricative voiceless aspirated 
                                                1.03904251 
                        labial-velar approximant voiceless 
                                                1.04219205 
                       alveolar plosive voiced palatalised 
                                                1.04987146 
                         bilabial nasal voiced palatalised 
                                                1.05659394 
                                   palatal nasal voiceless 
                                                1.05988443 
                                retroflex affricate voiced 
                                                1.06864948 
                        palatal lateral_approximant voiced 
                                                1.07337652 
                                 velar affricate voiceless 
                                                1.07434318 
                                  velar approximant voiced 
                                                1.07850441 
                         alveolar trill voiced palatalised 
                                                1.09206800 
                                       uvular nasal voiced 
                                                1.09693740 
                          velar plosive voiced palatalised 
                                                1.10531816 
          bilabial plosive voiceless aspirated palatalised 
                                                1.11164068 
                                  palatal fricative voiced 
                                                1.11656029 
                      velar fricative voiceless labialised 
                                                1.14280514 
                        velar plosive voiceless labialised 
                                                1.14702314 
          alveolar plosive voiceless aspirated palatalised 
                                                1.15420516 
                         alveolar plosive voiced aspirated 
                                                1.15841338 
                     velar fricative voiceless palatalised 
                                                1.16092832 
                        retroflex plosive voiced aspirated 
                                                1.16136282 
                       bilabial plosive voiced palatalised 
                                                1.16229042 
                                uvular affricate voiceless 
                                                1.16250784 
                     alveolar plosive voiceless labialised 
                                                1.16387110 
               postalveolar fricative voiceless labialised 
                                                1.17061667 
                         labial-palatal approximant voiced 
                                                1.17374232 
           alveolar plosive voiceless aspirated labialised 
                                                1.17789105 
                    uvular fricative voiceless palatalised 
                                                1.17800066 
                            velar nasal voiced palatalised 
                                                1.18107345 
                                       uvular trill voiced 
                                                1.19249105 
                           retroflex approximant voiceless 
                                                1.19405557 
                  alveolar fricative voiceless palatalised 
                                                1.20385641 
              velar plosive voiceless aspirated labialised 
                                                1.20466723 
                             velar nasal voiced labialised 
                                                1.20616447 
                       velar plosive voiceless glottalised 
                                                1.20703421 
                  alveolar fricative voiceless glottalised 
                                                1.21174057 
               labial-velar approximant voiced palatalised 
                                                1.21659371 
                     alveolar fricative voiced palatalised 
                                                1.22033503 
                 postalveolar affricate voiced palatalised 
                                                1.22299121 
           velar plosive voiceless glottalised palatalised 
                                                1.22471642 
          alveolar lateral_fricative voiceless glottalised 
                                                1.22538859 
                                 bilabial affricate voiced 
                                                1.22945430 
               postalveolar affricate voiceless labialised 
                                                1.23242119 
                     bilabial plosive voiceless labialised 
                                                1.23276242 
                               pharyngeal fricative voiced 
                                                1.23338945 
                  alveolar affricate voiceless palatalised 
                                                1.23638708 
                                 alveolar implosive voiced 
                                                1.24440617 
                 postalveolar fricative voiced palatalised 
                                                1.24468387 
                                    alveolar tap voiceless 
                                                1.24524328 
         alveolar plosive voiceless glottalised labialised 
                                                1.24621379 
              postalveolar fricative voiceless palatalised 
                                                1.24784730 
                    alveolar plosive voiceless glottalised 
                                                1.24839808 
                              epiglottal plosive voiceless 
                                                1.24941077 
           alveolo-palatal affricate voiceless glottalised 
                                                1.25448156 
                  postalveolar fricative voiced labialised 
                                                1.25506704 
                               epiglottal fricative voiced 
                                                1.25972863 
                     palatal plosive voiceless glottalised 
                                                1.26101781 
                alveolar approximant voiceless palatalised 
                                                1.26125378 
        alveolar lateral_approximant voiceless palatalised 
                                                1.26125378 
                          palatal plosive voiced aspirated 
                                                1.26290734 
                                retroflex fricative voiced 
                                                1.26723950 
                   alveolar fricative voiceless labialised 
                                                1.26830002 
     postalveolar affricate voiceless aspirated labialised 
                                                1.26830002 
                     palatal affricate voiceless aspirated 
                                                1.27021474 
          uvular plosive voiceless glottalised palatalised 
                                                1.27146179 
                      uvular plosive voiceless palatalised 
                                                1.27146179 
                   postalveolar affricate voiced aspirated 
                                                1.27966661 
              postalveolar affricate voiceless palatalised 
                                                1.28047817 
                    uvular affricate voiceless glottalised 
                                                1.28513222 
                           alveolar tap voiced palatalised 
                                                1.28597398 
                          uvular plosive voiced labialised 
                                                1.29688962 
                          bilabial nasal voiced labialised 
                                                1.29712444 
                       velar fricative voiceless aspirated 
                                                1.29930737 
                  postalveolar affricate voiced labialised 
                                                1.30285073 
               labiodental fricative voiceless palatalised 
                                                1.30592818 
                               alveolar lateral_tap voiced 
                                                1.30603243 
                                 bilabial implosive voiced 
                                                1.30777802 
                           velar plosive voiced labialised 
                                                1.30795664 
                      bilabial nasal voiceless palatalised 
                                                1.30814967 
                alveolo-palatal affricate voiced aspirated 
                                                1.30898736 
          alveolar lateral_fricative voiceless palatalised 
                                                1.30933273 
                   retroflex lateral_approximant voiceless 
                                                1.30974860 
                 labial-velar approximant voiced aspirated 
                                                1.31150874 
                             alveolar tap voiced aspirated 
                                                1.31160919 
           uvular plosive voiceless glottalised labialised 
                                                1.31208365 
                                    velar affricate voiced 
                                                1.31414183 
                     bilabial fricative voiced palatalised 
                                                1.31808227 
                  interdental fricative voiced palatalised 
                                                1.32588044 
           hissing-hushing fricative voiceless glottalised 
                                                1.33143865 
                          hissing-hushing affricate voiced 
                                                1.33150337 
               hissing-hushing affricate voiced labialised 
                                                1.33150337 
                       hissing-hushing affricate voiceless 
                                                1.33150337 
            hissing-hushing affricate voiceless labialised 
                                                1.33150337 
               hissing-hushing fricative voiced labialised 
                                                1.33150337 
            hissing-hushing fricative voiceless labialised 
                                                1.33150337 
                            epiglottal fricative voiceless 
                                                1.34408143 
                     alveolar approximant voiced aspirated 
                                                1.34580378 
             alveolar lateral_approximant voiced aspirated 
                                                1.34580378 
                      uvular affricate voiceless aspirated 
                                                1.35056207 
             uvular plosive voiceless aspirated labialised 
                                                1.35450335 
                        velar fricative voiced palatalised 
                                                1.35460603 
                              velar nasal voiced aspirated 
                                                1.35757578 
                postalveolar fricative voiceless aspirated 
                                                1.36185335 
                      uvular plosive voiceless glottalised 
                                                1.36194097 
                          hissing-hushing fricative voiced 
                                                1.36556980 
                       hissing-hushing fricative voiceless 
                                                1.36556980 
                      alveolar trill voiceless palatalised 
                                                1.37154949 
                        uvular fricative voiced labialised 
                                                1.37176674 
                     uvular fricative voiceless labialised 
                                                1.37403400 
                           labiodental affricate voiceless 
                                                1.37556323 
                  labiodental fricative voiced palatalised 
                                                1.37593611 
                     alveolar affricate voiced palatalised 
                                                1.38148157 
                       alveolar affricate voiced aspirated 
                                                1.38201419 
                   retroflex fricative voiceless aspirated 
                                                1.38369971 
                    retroflex fricative voiced palatalised 
                                                1.38535241 
                     alveolar fricative voiced glottalised 
                                                1.38584609 
             alveolar lateral_fricative voiced glottalised 
                                                1.38584609 
                       alveolar plosive voiced glottalised 
                                                1.38584609 
           alveolo-palatal fricative voiceless glottalised 
                                                1.38584609 
                  interdental fricative voiced glottalised 
                                                1.38584609 
               interdental fricative voiceless glottalised 
                                                1.38584609 
              postalveolar fricative voiceless glottalised 
                                                1.38584609 
               labiodental fricative voiceless glottalised 
                                                1.38738274 
                        alveolar plosive voiced labialised 
                                                1.38749884 
             hissing-hushing affricate voiceless aspirated 
                                                1.38749884 
  hissing-hushing affricate voiceless aspirated labialised 
                                                1.38749884 
           hissing-hushing affricate voiceless glottalised 
                                                1.38749884 
hissing-hushing affricate voiceless glottalised labialised 
                                                1.38749884 
                 pharyngeal fricative voiceless labialised 
                                                1.38749884 
                 retroflex affricate voiceless glottalised 
                                                1.38749884 
                       uvular fricative voiced palatalised 
                                                1.38749884 
         bilabial plosive voiceless glottalised labialised 
                                                1.38762263 
hissing-hushing fricative voiceless glottalised labialised 
                                                1.38762263 
                           alveolar trill voiced aspirated 
                                                1.38823983 
   postalveolar affricate voiceless glottalised labialised 
                                                1.39077986 
             alveolar lateral_fricative voiced palatalised 
                                                1.39218661 
         alveolar affricate voiceless aspirated labialised 
                                                1.39400692 
       alveolar affricate voiceless glottalised labialised 
                                                1.39400692 
                   alveolar affricate voiceless labialised 
                                                1.39400692 
                      alveolar fricative voiced labialised 
                                                1.39400692 
           alveolar lateral_fricative voiceless labialised 
                                                1.39400692 
                    epiglottal fricative voiced labialised 
                                                1.39400692 
                 epiglottal fricative voiceless labialised 
                                                1.39400692 
        bilabial plosive voiceless glottalised palatalised 
                                                1.39407349 
            uvular plosive voiceless aspirated palatalised 
                                                1.39407349 
                       velar affricate voiceless aspirated 
                                                1.39427886 
                     velar affricate voiceless glottalised 
                                                1.39427886 
                         velar fricative voiced labialised 
                                                1.39468633 
             alveolo-palatal fricative voiceless aspirated 
                                                1.39470218 
                pharyngeal fricative voiceless palatalised 
                                                1.39631109 
                         velar nasal voiceless palatalised 
                                                1.39808553 
                              bilabial affricate voiceless 
                                                1.39886979 
                    bilabial plosive voiceless glottalised 
                                                1.40181140 
                      alveolar nasal voiceless palatalised 
                                                1.40302047 
                       alveolar implosive voiced aspirated 
                                                1.40393995 
                      palatal approximant voiced aspirated 
                                                1.40393995 
             labiodental approximant voiceless palatalised 
                                                1.40457770 
                        alveolar tap voiceless palatalised 
                                                1.40531807 
                    labiodental fricative voiced aspirated 
                                                1.41824174 
                                  palatal implosive voiced 
                                                1.41827887 
                          retroflex nasal voiced aspirated 
                                                1.41827887 
                                    velar implosive voiced 
                                                1.41827887 
                                   uvular affricate voiced 
                                                1.42059823 
                            retroflex tap voiced aspirated 
                                                1.42576543 
           bilabial plosive voiceless aspirated labialised 
                                                1.42769547 
                                  labiodental nasal voiced 
                                                1.42770071 
                 labiodental affricate voiceless aspirated 
                                                1.42790225 
                     palatal fricative voiceless aspirated 
                                                1.42873434 
                        bilabial plosive voiced labialised 
                                                1.42873661 
        alveolar fricative voiceless aspirated palatalised 
                                                1.42888216 
                      retroflex affricate voiced aspirated 
                                                1.42896526 
                     bilabial affricate voiced palatalised 
                                                1.42934083 
    postalveolar affricate voiceless aspirated palatalised 
                                                1.42951067 
                      labial-palatal approximant voiceless 
                                                1.42965584 
                                   retroflex tap voiceless 
                                                1.42997454 
                         palatal-velar fricative voiceless 
                                                1.43041426 
                       uvular plosive voiceless labialised 
                                                1.43621809 
            velar plosive voiceless glottalised labialised 
                                                1.43621809 
                           bilabial nasal voiced aspirated 
                                                1.44194367 
                              labiodental affricate voiced 
                                                1.45247466 
                           alveolar nasal voiced aspirated 
                                                1.46833215 
              postalveolar affricate voiceless glottalised 
                                                1.57208744 
                   labiodental fricative voiced labialised 
                                                1.59322021 
                labiodental fricative voiceless labialised 
                                                1.59322021 
                  alveolar affricate voiceless glottalised 
                                                1.70704000