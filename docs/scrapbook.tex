\documentclass[11pt,a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage{fancyvrb}
\usepackage{listings}
\usepackage{booktabs}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{tipa}
\usepackage{courier}
\usepackage{natbib}
\usepackage{longtable}

\setcitestyle{aysep={}}
\setcitestyle{notesep={: }}

\title{Notes for `Areal dependence of consonant inventories'}

\author{}
\date{}

\begin{document}


\maketitle

\begin{table}[ht]
\caption{Sample sizes}
\label{sample-sizes}

\begin{minipage}{.5\linewidth}
    % \caption{By phylum}
    \centering
    \begin{tabular}{@{}lll@{}}
    \toprule
    Common phylum\\\textbackslash~Neighbours & $+$  & $-$   \\ \midrule
    $+$                                     & 1644 & 15533 \\
    $-$                                     & 1134 & 97129 \\ \bottomrule
    \end{tabular}
\end{minipage}%
\begin{minipage}{.5\linewidth}
    % \caption{By Genus}
    \centering
    \begin{tabular}{@{}lll@{}}
    \toprule
    Common genus\\\textbackslash~Neighbours & $+$  & $-$    \\ \midrule
    $+$                                    & 754  & 1583   \\
    $-$                                    & 2024 & 111079 \\ \bottomrule
    \end{tabular}
\end{minipage}
\end{table}

\begin{equation}
    \operatorname{Jaccard} = 1 - \dfrac{|A \cap B|}{|A \cup B|}
\end{equation}%
where A and B are sets of phonemes found in the respective languages.

Jaccard metric does not take into account a common process of phoneme splitting due to development of VOT distinctions or additional articulations, such as aspiration and glottalisation. This leads to the fact that there is no difference between, on the one hand, a pair of inventories that share some core of segments, while one of them enriched this core by developing some additional articulation (a simplified example could look like /p, t, k/ vs. /p, t, k, p\textsuperscript{h}, t\textsuperscript{h}, k\textsuperscript{h}/), and on the other hand, a pair of inventories sharing a common core, beyond which the rest of the phonemes are absolutely different (/p, t, k/ vs. /p, t, k, \textipa{P, Q, X}/.

In order to overcome this limitation, a new metric is proposed, \textit{Closest-Relative Cumulative Jaccard Dissimilarity} (CRCJ). It is computed in the following way: given inventories $I_1$ and $I_2$ where each phoneme is described as a set of IPA features, for each phoneme $p$ in $I_1$ we find its closest relative in $I_2$---that is, the phoneme in $I_2$ having the smallest Jaccard dissimilarity from $p$---sum these miminal Jaccard dissimilarities and then repeat the procedure starting with $I_2$. Formally, the metric can be defined in the following way:

\begin{equation}
    CRCJ = \sum_{p \in I_1}\operatorname{Jaccard}(p, q) + \sum_{r \in I_2}\operatorname{Jaccard}(r, s)
\end{equation}%
where \[q = \underset{x \in I_2}{\operatorname{argmin}}(\operatorname{Jaccard}(p, x))\] and \[s = \underset{x \in I_1}{\operatorname{argmin}}(\operatorname{Jaccard}(r, x))\]

In order to test the hypothesis about the dependence of consonant inventories on the inventories of neighbouring languages, we compute Jaccard and CRCJ dissimilarity for all pairs of languages in the data sample, separated into six groups:

\begin{enumerate}
    \item Not neighbours, do not belong to the same phylum (0.0.0)
    \item Neighbours, do not belong to the same phylum (1.0.0)
    \item Not neighbours, belong to the same phylum, but not to the same genus (0.1.0)
    \item Not neighbours, belong to the same genus (0.1.1)
    \item Neighbours, belong to the same phylum, but not to the same genus (1.1.0)
    \item Neighbours, belong to the same genus (1.1.1)
\end{enumerate}

Distributions for Jaccard and CRCJ dissimilarity values for these six groups are presented in Figures~\ref{fig:jaccard-distances} and \ref{fig:CRCJ-distances} respectively. We see that distributions of CRCJ values have smaller variances. Also it is remarkable that while Jaccard distributions are right heavy (they have a long left tail consisting of outlying pairs having low dissimilarity), CRCJ distributions are left heavy (with long right tails consisting of several language pairs with unusually big distance between them) and it points to the fact that Jaccard metric probably overestimates differences between inventories.

As the distributions in question are not normal, we used Kruskal-Wallis test to check if there are significant differences between groups and then conducted a post-hoc analysis of group differences using Dunn test.

In the case of Jaccard dissimilarity, Kruskas-Wallis test showed significant differences between groups, and the post-hoc analysis confirmed that all between-group differences are significant as well (the results of the test are presented in Listing~\ref{lst:jaccard}).

Results of tests of between-group differences in CRCJ values are the same, except for the fact that there is no significant difference between groups 1.0.0 and 1.1.0 ($p = 0.055$, Listing~\ref{lst:crcj}). This seems to indicate that neighbouring languages from different phyla show the same dynamics of segment-inventory development and that this development mostly consists of acquiring distinctions in VOT and additional articulations (in line with the analysis presented by Lindblom and Maddieson \citeyear{lindblom_maddieson_1988}).

Based on the results of the post-hocs tests and the differences between median dissimilarity values, we may formulate the following hierarchy:

\[1.1.1 < 0.1.1 < (1.1.0, 1.0.0) < 0.1.0 < 0.0.0\]

Most importantly, it shows that (i)~non-neighbouring languages from the same phylum but not the same genus tend to be more phonologically different between themselves than unrelated neighbouring languages; and (ii)~neighbouring languages from the same genus tend to be significantly more similar to each other phonological than non-neighbouring languages from the same genus.

In other words, it may be stated that the effect of language contact is stronger than the phylogenetic effect of phylum and that it retains significance over and above that of genus.

\begin{figure}{}
    \centering{
        \includegraphics[width=\textwidth]{images/Jaccard_vals_distributions.pdf}
    }    
    \caption{Distribution of Jaccard distances}
    \label{fig:jaccard-distances}
\end{figure}


\begin{figure}
    \centering{
        \includegraphics[width=\textwidth]{images/CRCJ_vals_distributions.pdf}
    }
    \caption{Distribution of CRCJ distances}
    \label{fig:CRCJ-distances}
\end{figure}

% \texttt{> sort(mediancs_jacc_vec)
% 1.1.1 0.1.1 1.1.0 1.0.0 0.1.0 0.0.0 
% 0.512 0.577 0.641 0.675 0.711 0.750}

% \texttt{> sort(mediancs_crcj_vec)
% 1.1.1  0.1.1  1.0.0  1.1.0  0.1.0  0.0.0 
% 6.895  8.881  9.687  9.933 11.895 12.367}

% \texttt{> logreg1.auc
%  [1] 0.9351074 0.9362539 0.9408785 0.9303700 0.9277387 0.9321960 0.9374166 0.9308024 0.9358879 0.9315729
% > logreg2.auc
%  [1] 0.9540110 0.9540716 0.9580779 0.9491816 0.9475784 0.9493266 0.9552686 0.9469547 0.9559954 0.9489023}

% \begin{figure}[tb]
%     \centering
%     \includegraphics[width=\textwidth]{scaled_log_distances.pdf}
%     \label{fig:log-scaled-dissimilarity}
%     \caption{Scaled log values for Jaccard and CRCJ dissimilarity values}
% \end{figure}



\bibliographystyle{unified.bst}
\bibliography{biblio}

\begin{lstlisting}[basicstyle=\small\ttfamily,frame=single,caption=Dunn test of group differences in Jaccard dissimularity values,label={lst:jaccard}]
> dunnTest(jaccard ~ interaction_var, data = jacc.data)
Dunn (1964) Kruskal-Wallis multiple comparison
p-values adjusted with the Holm method.

      Comparison          Z       P.unadj         P.adj
1  0.0.0 - 0.1.0  41.474157  0.000000e+00  0.000000e+00
2  0.0.0 - 0.1.1  48.790291  0.000000e+00  0.000000e+00
3  0.1.0 - 0.1.1  31.017781 3.104022e-211 3.724826e-210
4  0.0.0 - 1.0.0  18.042627  9.015441e-73  8.113896e-72
5  0.1.0 - 1.0.0   5.469031  4.525015e-08  1.357504e-07
6  0.1.1 - 1.0.0 -15.602059  7.048371e-55  5.638697e-54
7  0.0.0 - 1.1.0  23.058187 1.217470e-117 1.217470e-116
8  0.1.0 - 1.1.0  11.760620  6.227506e-32  3.736504e-31
9  0.1.1 - 1.1.0  -8.548532  1.246643e-17  4.986572e-17
10 1.0.0 - 1.1.0   5.304271  1.131243e-07  2.262485e-07
11 0.0.0 - 1.1.1  34.149319 1.368650e-255 1.779244e-254
12 0.1.0 - 1.1.1  23.507714 3.401293e-122 3.741422e-121
13 0.1.1 - 1.1.1   2.921103  3.487944e-03  3.487944e-03
14 1.0.0 - 1.1.1  15.099826  1.623606e-51  1.136524e-50
15 1.1.0 - 1.1.1   9.536330  1.479766e-21  7.398829e-21
\end{lstlisting}

\begin{lstlisting}[basicstyle=\small\ttfamily,frame=single,caption=Dunn test of group differences in CRCJ dissimularity values,label={lst:crcj}]
> dunnTest(CRCJ ~ interaction_var, data = jacc.data)
Dunn (1964) Kruskal-Wallis multiple comparison
p-values adjusted with the Holm method.

      Comparison          Z       P.unadj         P.adj
1  0.0.0 - 0.1.0 15.4532125  7.177517e-54  6.459765e-53
2  0.0.0 - 0.1.1 31.9915914 1.427411e-224 2.141117e-223
3  0.1.0 - 0.1.1 24.6569554 3.099339e-134 3.719206e-133
4  0.0.0 - 1.0.0 19.5534975  3.851861e-85  4.237047e-84
5  0.1.0 - 1.0.0 14.4629290  2.077648e-47  1.662119e-46
6  0.1.1 - 1.0.0 -4.0682344  4.737072e-05  9.474144e-05
7  0.0.0 - 1.1.0 16.5490735  1.625855e-61  1.625855e-60
8  0.1.0 - 1.1.0 12.1396675  6.508999e-34  4.556299e-33
9  0.1.1 - 1.1.0 -4.4174006  9.989499e-06  2.996850e-05
10 1.0.0 - 1.1.0 -0.5977382  5.500146e-01  5.500146e-01
11 0.0.0 - 1.1.1 30.3354935 3.903654e-202 5.465116e-201
12 0.1.0 - 1.1.1 25.9928899 5.959149e-149 7.746894e-148
13 0.1.1 - 1.1.1  8.6751610  4.129657e-18  1.651863e-17
14 1.0.0 - 1.1.1 11.1723724  5.567374e-29  3.340424e-28
15 1.1.0 - 1.1.1 11.1475996  7.356410e-29  3.678205e-28
\end{lstlisting}

\footnotesize{
\begin{longtable}{lr}
\caption{Drop in residual deviance}\label{tbl:residual-deviance}\\
\hline\hline
\multicolumn{1}{l}{results_df.tex}&\multicolumn{1}{c}{Phoneme}&\multicolumn{1}{c}{Drop in residual deviance}&\multicolumn{1}{c}{p_vals}\tabularnewline
\hline
retroflex tap voiced&retroflex tap voiced&$3.88564563460603e+01$&$0.000$\tabularnewline
retroflex plosive voiceless aspirated&retroflex plosive voiceless aspirated&$3.11305834222146e+01$&$0.000$\tabularnewline
retroflex plosive voiceless&retroflex plosive voiceless&$2.75733172698221e+01$&$0.000$\tabularnewline
retroflex plosive voiced&retroflex plosive voiced&$2.06580575560413e+01$&$0.000$\tabularnewline
retroflex fricative voiced&retroflex fricative voiced&$1.92341647608295e+01$&$0.000$\tabularnewline
retroflex fricative voiceless&retroflex fricative voiceless&$1.72884600842711e+01$&$0.000$\tabularnewline
velar plosive voiced aspirated&velar plosive voiced aspirated&$1.66472353871499e+01$&$0.000$\tabularnewline
uvular plosive voiceless&uvular plosive voiceless&$1.63624678644081e+01$&$0.000$\tabularnewline
interdental fricative voiceless&interdental fricative voiceless&$1.41193030262062e+01$&$0.000$\tabularnewline
velar fricative voiced&velar fricative voiced&$1.29086117933528e+01$&$0.000$\tabularnewline
retroflex plosive voiced aspirated&retroflex plosive voiced aspirated&$1.26839553557316e+01$&$0.000$\tabularnewline
alveolar fricative voiceless palatalised&alveolar fricative voiceless palatalised&$1.26338827052983e+01$&$0.000$\tabularnewline
bilabial plosive voiced aspirated&bilabial plosive voiced aspirated&$1.24509507691188e+01$&$0.000$\tabularnewline
retroflex affricate voiced&retroflex affricate voiced&$1.22282289529848e+01$&$0.000$\tabularnewline
retroflex affricate voiceless&retroflex affricate voiceless&$1.01375055227621e+01$&$0.001$\tabularnewline
retroflex affricate voiceless aspirated&retroflex affricate voiceless aspirated&$9.84385289629590e+00$&$0.002$\tabularnewline
alveolo-palatal fricative voiceless&alveolo-palatal fricative voiceless&$9.81103017224655e+00$&$0.002$\tabularnewline
interdental fricative voiced&interdental fricative voiced&$9.72817930660638e+00$&$0.002$\tabularnewline
bilabial plosive voiceless glottalised&bilabial plosive voiceless glottalised&$9.23786590499161e+00$&$0.002$\tabularnewline
labiodental fricative voiced&labiodental fricative voiced&$8.94594004309812e+00$&$0.003$\tabularnewline
velar fricative voiceless&velar fricative voiceless&$8.50722197829691e+00$&$0.004$\tabularnewline
bilabial fricative voiced&bilabial fricative voiced&$8.19386741537075e+00$&$0.004$\tabularnewline
alveolo-palatal affricate voiceless aspirated&alveolo-palatal affricate voiceless aspirated&$7.27846722316085e+00$&$0.007$\tabularnewline
labiodental fricative voiceless&labiodental fricative voiceless&$6.65182738042517e+00$&$0.010$\tabularnewline
alveolar lateral_approximant voiced&alveolar lateral_approximant voiced&$6.34106335432116e+00$&$0.012$\tabularnewline
alveolar approximant voiced&alveolar approximant voiced&$6.19106444664035e+00$&$0.013$\tabularnewline
alveolar approximant voiced palatalised&alveolar approximant voiced palatalised&$5.94826084317320e+00$&$0.015$\tabularnewline
alveolar lateral_approximant voiced palatalised&alveolar lateral_approximant voiced palatalised&$5.94826084317320e+00$&$0.015$\tabularnewline
alveolo-palatal fricative voiced&alveolo-palatal fricative voiced&$5.66552958060197e+00$&$0.017$\tabularnewline
postalveolar fricative voiced&postalveolar fricative voiced&$5.61292926482221e+00$&$0.018$\tabularnewline
alveolar tap voiced&alveolar tap voiced&$5.34160217234501e+00$&$0.021$\tabularnewline
alveolar nasal voiceless&alveolar nasal voiceless&$5.12263965922766e+00$&$0.024$\tabularnewline
postalveolar fricative voiceless&postalveolar fricative voiceless&$4.69181208393479e+00$&$0.030$\tabularnewline
postalveolar affricate voiced&postalveolar affricate voiced&$4.61532631855096e+00$&$0.032$\tabularnewline
alveolar plosive voiceless aspirated&alveolar plosive voiceless aspirated&$4.24771271415398e+00$&$0.039$\tabularnewline
palatal nasal voiceless&palatal nasal voiceless&$3.99643753308138e+00$&$0.046$\tabularnewline
bilabial plosive voiceless aspirated&bilabial plosive voiceless aspirated&$3.83385287371982e+00$&$0.050$\tabularnewline
alveolar fricative voiceless&alveolar fricative voiceless&$3.28948221141056e+00$&$0.070$\tabularnewline
uvular fricative voiced&uvular fricative voiced&$3.22742409094695e+00$&$0.072$\tabularnewline
alveolar fricative voiced&alveolar fricative voiced&$3.21314458141148e+00$&$0.073$\tabularnewline
retroflex nasal voiced&retroflex nasal voiced&$2.86185152033795e+00$&$0.091$\tabularnewline
glottal fricative voiced&glottal fricative voiced&$2.80237144088741e+00$&$0.094$\tabularnewline
alveolar affricate voiceless aspirated&alveolar affricate voiceless aspirated&$2.75037360123065e+00$&$0.097$\tabularnewline
alveolo-palatal affricate voiceless&alveolo-palatal affricate voiceless&$2.56941992655450e+00$&$0.109$\tabularnewline
alveolar lateral_fricative voiceless&alveolar lateral_fricative voiceless&$2.51611452449879e+00$&$0.113$\tabularnewline
uvular fricative voiceless&uvular fricative voiceless&$2.28214695329333e+00$&$0.131$\tabularnewline
pharyngeal fricative voiceless&pharyngeal fricative voiceless&$2.25386462092624e+00$&$0.133$\tabularnewline
palatal approximant voiced&palatal approximant voiced&$2.24789620759594e+00$&$0.134$\tabularnewline
bilabial plosive voiced&bilabial plosive voiced&$2.22172698200168e+00$&$0.136$\tabularnewline
alveolar nasal voiced&alveolar nasal voiced&$2.14346010979011e+00$&$0.143$\tabularnewline
alveolar affricate voiceless&alveolar affricate voiceless&$2.02874974554771e+00$&$0.154$\tabularnewline
velar nasal voiced&velar nasal voiced&$2.00621541664520e+00$&$0.157$\tabularnewline
alveolo-palatal affricate voiced&alveolo-palatal affricate voiced&$1.97635744558596e+00$&$0.160$\tabularnewline
velar plosive voiceless labialised&velar plosive voiceless labialised&$1.91821836822878e+00$&$0.166$\tabularnewline
velar nasal voiceless&velar nasal voiceless&$1.85327177219165e+00$&$0.173$\tabularnewline
palatal fricative voiceless&palatal fricative voiceless&$1.84977705734011e+00$&$0.174$\tabularnewline
uvular plosive voiceless aspirated&uvular plosive voiceless aspirated&$1.79205522187011e+00$&$0.181$\tabularnewline
velar plosive voiceless aspirated&velar plosive voiceless aspirated&$1.67637200696146e+00$&$0.195$\tabularnewline
bilabial plosive voiced palatalised&bilabial plosive voiced palatalised&$1.50208866614359e+00$&$0.220$\tabularnewline
bilabial nasal voiceless&bilabial nasal voiceless&$1.49441055257795e+00$&$0.222$\tabularnewline
palatal nasal voiced&palatal nasal voiced&$1.12582491407244e+00$&$0.289$\tabularnewline
postalveolar affricate voiceless&postalveolar affricate voiceless&$1.11496362639690e+00$&$0.291$\tabularnewline
velar plosive voiceless&velar plosive voiceless&$9.73963293377473e-01$&$0.324$\tabularnewline
palatal plosive voiced&palatal plosive voiced&$9.34981866699587e-01$&$0.334$\tabularnewline
velar plosive voiced&velar plosive voiced&$8.67691541409130e-01$&$0.352$\tabularnewline
retroflex lateral_approximant voiced&retroflex lateral_approximant voiced&$8.57406393362794e-01$&$0.354$\tabularnewline
alveolar trill voiceless&alveolar trill voiceless&$8.21277157556253e-01$&$0.365$\tabularnewline
alveolar trill voiced palatalised&alveolar trill voiced palatalised&$8.13087090425157e-01$&$0.367$\tabularnewline
glottal fricative voiceless&glottal fricative voiceless&$7.65519109753143e-01$&$0.382$\tabularnewline
bilabial plosive voiceless palatalised&bilabial plosive voiceless palatalised&$6.38183269074560e-01$&$0.424$\tabularnewline
alveolar plosive voiced&alveolar plosive voiced&$5.97067826259888e-01$&$0.440$\tabularnewline
retroflex approximant voiced&retroflex approximant voiced&$5.11102518644520e-01$&$0.475$\tabularnewline
glottal plosive&glottal plosive&$4.89234158600539e-01$&$0.484$\tabularnewline
palatal fricative voiced&palatal fricative voiced&$4.66246276247517e-01$&$0.495$\tabularnewline
alveolar trill voiced&alveolar trill voiced&$4.61532769393330e-01$&$0.497$\tabularnewline
labial-velar approximant voiced&labial-velar approximant voiced&$4.23324072604714e-01$&$0.515$\tabularnewline
palatal plosive voiceless aspirated&palatal plosive voiceless aspirated&$4.11577353204990e-01$&$0.521$\tabularnewline
bilabial nasal voiced&bilabial nasal voiced&$4.10095873094612e-01$&$0.522$\tabularnewline
alveolar affricate voiced&alveolar affricate voiced&$3.64412526492117e-01$&$0.546$\tabularnewline
bilabial plosive voiceless&bilabial plosive voiceless&$3.34500450570378e-01$&$0.563$\tabularnewline
alveolar nasal voiced palatalised&alveolar nasal voiced palatalised&$2.74393623661922e-01$&$0.600$\tabularnewline
labiodental approximant voiceless&labiodental approximant voiceless&$1.90069761845692e-01$&$0.663$\tabularnewline
alveolar plosive voiceless&alveolar plosive voiceless&$1.62245392061379e-01$&$0.687$\tabularnewline
palatal affricate voiceless aspirated&palatal affricate voiceless aspirated&$1.49390364536515e-01$&$0.699$\tabularnewline
palatal affricate voiceless&palatal affricate voiceless&$1.36818442146961e-01$&$0.711$\tabularnewline
bilabial nasal voiced palatalised&bilabial nasal voiced palatalised&$1.29844447940059e-01$&$0.719$\tabularnewline
palatal lateral_approximant voiced&palatal lateral_approximant voiced&$1.01544405483637e-01$&$0.750$\tabularnewline
palatal affricate voiced&palatal affricate voiced&$9.38547512878074e-02$&$0.759$\tabularnewline
velar plosive voiceless palatalised&velar plosive voiceless palatalised&$8.79021855679554e-02$&$0.767$\tabularnewline
postalveolar affricate voiceless aspirated&postalveolar affricate voiceless aspirated&$4.93762878320467e-02$&$0.824$\tabularnewline
alveolar plosive voiceless palatalised&alveolar plosive voiceless palatalised&$1.60426820772130e-02$&$0.899$\tabularnewline
palatal plosive voiceless&palatal plosive voiceless&$4.18024190349797e-03$&$0.948$\tabularnewline
alveolar approximant voiceless&alveolar approximant voiceless&$2.55429055182788e-03$&$0.960$\tabularnewline
alveolar lateral_approximant voiceless&alveolar lateral_approximant voiceless&$2.55429055182788e-03$&$0.960$\tabularnewline
uvular plosive voiced&uvular plosive voiced&$2.15664244509384e-03$&$0.963$\tabularnewline
alveolar plosive voiced palatalised&alveolar plosive voiced palatalised&$3.62667389879334e-05$&$0.995$\tabularnewline
\hline
\end{longtable}
}

\end{document}