\documentclass{beamer}

\usepackage{graphicx}
\usepackage{fontspec}
\usepackage{natbib}
\usepackage{subfigure}
\usepackage{listings}
\usefonttheme{serif}
\setmainfont{Georgia}
\setromanfont{Georgia}
\newfontfamily{\doulos}{DoulosSIL.ttf}

\setbeamertemplate{navigation symbols}{}%remove navigation symbols

\title{Predicting consonantal inventories of~the~languages of~Eurasia using phylogenetic and~areal data}
\author{Dmitry Nikolaev}
\institute{Dynamics of Language Lab HUJI}
\date{26 April 2017}

\begin{document}

\frame{\titlepage}

\begin{frame}[t]\frametitle{Research question}
	
	Is it possible to predict the consonant inventory of a language? How do we do this and how do we estimate how successful we are?

\end{frame}

\begin{frame}[t]\frametitle{Proposed (minor) correlates for different characteristics of inventories}
	\begin{itemize}
		\item Distance from Africa \citep{Atkinson2011,Jaeger2011}
		\item Average annual temperature and sexual freedom \citep{Fought2004,Ember2007}
		\item Population size \citep{Hay2007,Atkinson2011,Wichmann2011,Nettle2012,Donohue2011,Moran2012}
		\item Altitude \citep{Everett2013}
		\item and more, see \citep{Ladd2015}
	\end{itemize}

	Correlations may be robust and significant in some cases, but they are not very strong.
\end{frame}

\begin{frame}[t]\frametitle{Major suspects}
	\begin{itemize}
		\item Phylogenetic informantion
		\item Areal data
	\end{itemize}
\end{frame}

\begin{frame}[t]\frametitle{Phylogenetics is easy}

	As long as everyone agrees on the family membership for languages. Long-range relatedness sometimes suspected as a factor for the distribution of typological variables, but nobody really bothers. Seems to be unimportant for segmental inventories.

\end{frame}

\begin{frame}[t]\frametitle{Phylogenetics is problematic}

	Families are of vastly differing sizes and possibly time depths. It would be ideal to compare oranges to oranges, but many large families lack good internal classifications.

    Small families and isolates tend to kill statistical significance. This is unfortunate for model building and even more unfortunate for predicting real stuff.

\end{frame}

\begin{frame}[t]\frametitle{Areal is problematic}
	\begin{itemize}
		\item Usually not enough data (dense sampling is for dialectologists)
		\item Areality is hard to quantify
	\end{itemize}
\end{frame}

\begin{frame}[t]\frametitle{New available data on phonologies of Eurasia}

	\begin{itemize}
		\item PHOIBLE
		\item The Database of Eurasian Phonological Inventories (EURPhon)
	\end{itemize}

	Together they provide data for c. 490 languages. Looks dense enough for practical purposes.
\end{frame}

\begin{frame}[t]\frametitle{Quantifying areality I: neighbour graphs}

	\begin{itemize}
		\item Real distances between languages are nearly impossible to establish---does not seem reasonable to include this as a variable.
		\item An alternative---a neighbour graph with languages as vertices. Possible methods for constructing it:
			\begin{itemize}
				\item Languages are connected by edges if the distance between their points does not exceed a certain threshold
				\item Delaunay triangulation with pruning of extra-lengthy edges
			\end{itemize}
	\end{itemize}

\end{frame}

\begin{frame}[t]\frametitle{Delaunay triangulation}
    Delaunay triangulation of a set of points is their division into triangles in such a way that circumcircles of the resulting triangles do not contain any other points other than the basic $3$. Close to the intuitive notion of `neighbour joining'.
    \begin{figure}
    	\centering
	    \includegraphics[scale=0.37]{dt.pdf}
    \end{figure}
\end{frame}

\begin{frame}[t]\frametitle{DT on a sphere}
    DT is usually performed as if languages are on a plain based on (possibly transformed) latitude and longitude. Longitude gets flipped in the North East Eurasia, and from that area languages loose their neighbours.\\
	\vspace{5mm}	
    However, DT on a sphere is simply a convex hull of the points. Therefore, we can project languages onto a unit sphere, find the convex hull for these points, and discard edges that are too long ($>1000$ km).
\end{frame}

\begin{frame}[t]\frametitle{Neighbour graph of Eurasian languages used for this study}
    \includegraphics[width=\textwidth]{images/neighbour_graph.png}
\end{frame}

\begin{frame}[t]\frametitle{Quantifying areality II: neighbour-pressure metric}
    Data on actual language contact are very scarce. We may assume that neighbouring languages should be in contact, but we cannot be sure that this is not so if their points are not connected with an edge in the neighbour graph.\\
	\vspace{5mm}
	Workaround: I assume that languages positively influence the probability of their neighbours' acquiring/retaining some feature, and this influence decreases exponentially with distance between them (measured in edges).
\end{frame}

\begin{frame}[t]\frametitle{NPM definition}

	NPM is computed separately for every feature of interested and is equal to

	\begin{equation} \label{npm}
	\sum_{l \in L, l \ne l_0} \frac{i_l}{2^{d_l}}
	\end{equation}

	where

	\begin{itemize}
		\item $L$ is the set of all languages in the sample
		\item $l_0$ is the language, for which the density measure is computed
		\item $i_l$ is equal to $0$ or $1$ according to whether a given language possesses the feature of interest
		\item $d_l$ is the minimal length of a path between $l_0$ and $l_i$ in the neighbour graph
	\end{itemize}

\end{frame}

\begin{frame}[t]\frametitle{An aside: density maps}
    NPM can be used to display areal densities of features on a map
    \begin{figure}[ht]
	\caption{Density map for a feature {\doulos \textsc{voiceless nasal}}}
	\includegraphics[width=\textwidth]{images/voiceless_nasal.png}
	\centering
	\end{figure}
\end{frame}

\begin{frame}[t]\frametitle{Enumerating phonemes}
    The majority of widely distributed consonants are captured by a combination of three features: place ({\doulos \textsc{bilabial, labiodental}}, etc.), manner ({\doulos \textsc{plosive, fricative, affricate}}), and VOT ({\doulos \textsc{voiced, voiceless}}). To incorporate additional groups of phonemes found in numerours Eurasian languages it is necessary to include features {\doulos \textsc{aspirated, palatalised, labialised}}, and {\doulos \textsc{glottalised}}. Contrastive phonemic consonant length is a relatively rare phenomenon and was not included in the analysis. Glottal stop includes only place and manner features since it is not specified for VOT.\\
    \vspace{5mm}
    Distributions of 270 phonemes were studied.
\end{frame}

\begin{frame}[t]\frametitle{The data frame}

	Columns:

	\begin{itemize}
		\item Language
		\item Phylum
		\item Feature specification of the phoneme
		\item Presence/absence of the phoneme
		\item NPM for the phoneme+language
	\end{itemize}

\end{frame}

\begin{frame}[t]\frametitle{Model fitting: a simplistic approach}

	A phoneme and phylum agnostic logistic model showed that NPM is a significant predictor ($p < 0.0001$) for the presence of a phoneme in a given inventory. However, as `languages in contact also tend to be related' \citep{Ladd2015}, it was necessary to check the dependence of the presence of phonemes on phyla.

\end{frame}

\begin{frame}[t]\frametitle{Random-intercept mixed model}

	A random-intercept logistic model was fitted with NPM as a fixed effect and interaction of phylum and phoneme as a random effect.\\
	\vspace{5mm}
	A random-intercept model assumes that different sub-populations (here---different phonemes in different phyla) have different baselines. I.e., if a phoneme is prone to contact-induced spread, it will have a lower baseline and a lower corresponding NPM value necessary to predict its presence in an inventory.

\end{frame}

\begin{frame}[t]\frametitle{An aside: random intercepts and random slopes}

	There is a richer class of models where different sub-populations may have different \textit{rates} of dependence on fixed effects. However, it is not necessary to assume such a complex scenario (and the respective model does not converge anyway). (In a logistic model, the straight lines fitted are log-odds, which can be then converted to probabilities.)

	\begin{figure}
		\centering
		\includegraphics[width=0.5\textwidth]{images/random_intercept.PNG}
		\includegraphics[width=0.5\textwidth]{images/random_slope.PNG}
	\end{figure}

	{\footnotesize (Illustrations taken from Ringdal (2013), http://essedunet.nsd.uib.no/cms/topics/multilevel/)}

\end{frame}

\begin{frame}[t]\frametitle{Results}

	\begin{itemize}
		\item NPM is still a significant predictor ($p < 0.0001$).
		\item Intercepts vary significantly, which suggests that a more nuanced approach is needed
	\end{itemize}

	\includegraphics[width=\textwidth]{images/interaction_intercepts.png}

\end{frame}

\begin{frame}[t]\frametitle{Predicting distributions}

	A separate model was fitted for all phonemes trying to predict their distribution based on NPM and phyla. Only rather widespread phonemes from major phyla could be used for these models: 432 languages and 234 phonemes in all.

	\begin{table}[ht]
	\centering
	% \caption{Number of languages from major families analysed in this study}
	\label{lang_count}
	\begin{tabular}{ll}
	\hline
	Phylum            & Number of languages \\ \hline
	Mongolic          & 13                  \\
	Tai-Kadai         & 18                  \\
	Austronesian      & 24                  \\
	Nakh-Daghestanian & 12                  \\
	Turkic            & 20                  \\
	Dravidian         & 34                  \\
	Uralic            & 32                  \\
	Mon-Khmer         & 36                  \\
	Sino-Tibetan      & 116                 \\
	Indo-European     & 126                 \\ \hline
	\end{tabular}
	\end{table}

\end{frame}

\begin{frame}[t]\frametitle{Model comparison I: likelihood-ratio tests}

	How can we know that one predictor is better than the other? One of the ways to test this in a rigorous way is use likelihood-ratio tests for nested models:

	\begin{enumerate}
		\item Fit a model with the first predictor.\pause
		\item Add another predictor and fit a second model.\pause
		\item If the second model explains more variance than the first one, the LR test can be used to estimate whether this is accidental.\pause
		\item Do the same in the reverse order in order to see which of the two predictors makes a bigger contribution.
	\end{enumerate}

\end{frame}

\begin{frame}[t]\frametitle{An ideal case}

    One predictor wins over the other. I.e., when we add the good predictor to the model based on the bad predictor, it makes the model significantly better, but if we add the bad predictor to the model based on the good predictor, model does not get significantly better.

\end{frame}

\begin{frame}[t]\frametitle{An example: voiced labiodental fricative}
    Model 1: NPM as a predictor. LogLik $= -240.94$

	Model 2: NPM and phylum as predictors. LogLik = $= -230.43$

	$p = 0.01259$\\
	\vspace{5mm}

	Model 1: phylum as a predictor. LogLik $= -254.79$

	Model 2: NPM and phylum as predictors. LogLik = $= -230.43$

	$p = 2.964e-12$\\
	\vspace{5mm}

	NPM seems to be a much better predictor than phylum although phylum is still able to explain a significant amount of residual variance.

\end{frame}

\begin{frame}[t]\frametitle{LR tests for all phonemes}

	For 234 phonemes, there were 65 cases where addition of NPM to the model did not significantly improve it. On the other hand, there were 166 cases where addition of phylum did not make any significant difference. (NB: some cases are mildly spurious as the algorithm did not converge due to the scarcity of data.)

\end{frame}

\begin{frame}[t,allowframebreaks]\frametitle{Phonemes that gain the least from adding NPM to the model}
\begin{itemize}
	\item alveolar lateral fricative voiceless palatalised
	\item alveolar plosive voiced
	\item alveolar plosive voiceless
	\item alveolar tap voiceless
	\item bilabial affricate voiced
	\item bilabial fricative voiced palatalised
	\item bilabial fricative voiceless
	\item epiglottal plosive voiceless
	\item glottal fricative voiced
	\item interdental fricative voiced palatalised
	\item labial-velar approximant voiceless
	\item postalveolar affricate voiceless glottalised labialised
	\item postalveolar affricate voiceless labialised
	\item postalveolar fricative voiced palatalised
	\item postalveolar fricative voiceless aspirated
	\item uvular plosive voiceless aspirated labialised
	\item uvular trill voiced
	\item velar approximant voiced
	\item velar fricative voiced palatalised
	\item velar nasal voiced aspirated
	\item velar nasal voiced labialised
	\item velar plosive voiced labialised
\end{itemize}
\end{frame}

\begin{frame}[t]\frametitle{But what about predicting?}
    
	Statistical significance is an important parameter, but it is only tangentially related to the ability of a model to actually predict distributions. Moreover, it is perfectly possible for a model to make good predictions using statistically insignificant predictors.

\end{frame}

\begin{frame}[t]\frametitle{Comparing models based on predictions}
    
	Logistic models discussed in this study produce probabilities of presence of a given phoneme in a given language and can be used as binary classifiers (which output $1$ or $0$). How do we compare those?

\end{frame}

\begin{frame}[t]\frametitle{The ROC-AUC metric}
	\begin{enumerate}
		\item Construct the `ROC' curve for different decision thresholds with True Positive Rate and False Positive Rate ($= 1 - TPR$) as axes.
		\item Estimate the area under the curve (AUC). It is close to $1$ (or $0$) for a good classifier, and to $0.5$ for a useless classifier.
	\end{enumerate}

	\includegraphics[width=0.5\textwidth]{images/binormal.png}
	\includegraphics[width=0.5\textwidth]{images/auroc_example.png}
\end{frame}

\begin{frame}[t]\frametitle{Train data vs. test data}
    
	In machine learning, one usually trains the model on one part of the data, and then estimates its performance on some other part of the data, randomly chosen (once, or several times---\textit{cross validation}). However, in our case it is tricky since we train separate models for all phonemes, and randomly chosen part of the data may have suprises. For instance, if, for some family only 3 of 30 languages have some phoneme, we can accidentally put all 3 in the train data and all 27 in the test data, or vice versa.

\end{frame}

\begin{frame}[t]\frametitle{LOOCV}
    
	An alternative---leave-one-out cross validation. We repeatedly train the model on all the data except for one data point and test it on that data. Eventually, all the data points are used as test data, and we construct ROC-AUC curves based on these predictions.

\end{frame}

\begin{frame}[t,allowframebreaks]\frametitle{LOOCV ROC-AUCS for predicting distributions based on NPM vs. phyla vs. both}
    Cf. the table
\end{frame}

\begin{frame}[t]\frametitle{Model-performance asymmetry}
    
	\includegraphics[width=\textwidth]{images/phylum_vs_density.png}

\end{frame}

\begin{frame}[t]\frametitle{From predicting distributions to predicting inventaries}
    For every language in the sample, train the model for all phonemes on all other languages (LOOCV) and try to predict if the language has them.
\end{frame}

\begin{frame}[t]\frametitle{Measuring performance: precision and recall}
    \begin{center}
	    \includegraphics[height=1.2\textheight]{images/Precisionrecallnew.pdf}
    \end{center}
\end{frame}

\begin{frame}[t]\frametitle{Precision recall, and F1 (their harmonic mean)}
	First row---phylum-based classifier; second row---NPM-based classifier:
    \begin{center}
	    \includegraphics[width=.95\textwidth]{images/phylum-vs-density-precision-recall-f1.png}
    \end{center}
\end{frame}

\begin{frame}[t]\frametitle{Not only NPM has a slight edge, but also}
   	we achieve similar precision/recall values with isolates and languages from small families, which cannot be tackled with phylum-based models at all.
    \includegraphics[width=\textwidth]{images/precision-recall-small-families.png}
\end{frame}

\begin{frame}[t]\frametitle{Conclusions}
    \begin{itemize}
    	\item NPM is a good predictor for geographical distributions of consonants and whole consonantal inventories. It beats phylogenetic info when used on its own and usually significantly improves phylum-based models.
    	\item It can be used in cases where phyla are uninformative.
    \end{itemize}
    Practical consequence: genetically-balanced phonological samples are useless if they are not spatially balanced.
\end{frame}

\begin{frame}[t]\frametitle{More work to do}
    \begin{itemize}
    	\item NPM is just one option for quantifying areality based on the neighbour graph. What are the alternatives?
    	\item Add more data (hopefully, the models will improve).
    	\item Holy Grail: create diachronical model that will predict the current distributions (or at least their general characteristics) from a set of basic assumptions.
    \end{itemize}
\end{frame}

\begin{frame}[t,allowframebreaks]\frametitle{Bibliography}
	\bibliographystyle{apalike}
	{\footnotesize \bibliography{refs}}
\end{frame}
\end{document}