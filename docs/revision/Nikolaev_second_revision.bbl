\begin{thebibliography}{60}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{#1}
\providecommand{\urlprefix}{}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi:\discretionary{}{}{}#1}\else
  \providecommand{\doi}{doi:\discretionary{}{}{}\begingroup
  \urlstyle{rm}\Url}\fi

\bibitem[{Abadi et~al.(2015)Abadi, Agarwal, Barham, Brevdo, Chen, Citro,
  Corrado, Davis, Dean, Devin, Ghemawat, Goodfellow, Harp, Irving, Isard, Jia,
  Jozefowicz, Kaiser, Kudlur, Levenberg, Man\'{e}, Monga, Moore, Murray, Olah,
  Schuster, Shlens, Steiner, Sutskever, Talwar, Tucker, Vanhoucke, Vasudevan,
  Vi\'{e}gas, Vinyals, Warden, Wattenberg, Wicke, Yu \&
  Zheng}]{tensorflow2015-whitepaper}
Abadi, Mart\'{\i}n, Ashish Agarwal, Paul Barham, Eugene Brevdo, Zhifeng Chen,
  Craig Citro, Greg~S. Corrado, Andy Davis, Jeffrey Dean, Matthieu Devin,
  Sanjay Ghemawat, Ian Goodfellow, Andrew Harp, Geoffrey Irving, Michael Isard,
  Yangqing Jia, Rafal Jozefowicz, Lukasz Kaiser, Manjunath Kudlur, Josh
  Levenberg, Dan Man\'{e}, Rajat Monga, Sherry Moore, Derek Murray, Chris Olah,
  Mike Schuster, Jonathon Shlens, Benoit Steiner, Ilya Sutskever, Kunal Talwar,
  Paul Tucker, Vincent Vanhoucke, Vijay Vasudevan, Fernanda Vi\'{e}gas, Oriol
  Vinyals, Pete Warden, Martin Wattenberg, Martin Wicke, Yuan Yu \& Xiaoqiang
  Zheng. 2015.
\newblock {TensorFlow}: Large-scale machine learning on heterogeneous systems.
\newblock Software available from tensorflow.org.
\newblock \urlprefix\url{https://www.tensorflow.org/}.

\bibitem[{Atkinson(2011)}]{Atkinson2011}
Atkinson, Q.~D. 2011.
\newblock Phonemic diversity supports a serial founder effect model of language
  expansion from africa.
\newblock \emph{Science} 332(6027). 346--349.
\newblock \doi{10.1126/science.1199295}.

\bibitem[{Bashir(2016)}]{bashir_contact_2016}
Bashir, Elena. 2016.
\newblock Contact and convergence.
\newblock In Hans~Henrich Hock \& Elena Bashir (eds.), \emph{The languages and
  linguistics of {South} {Asia}: a comprehensive guide} (The world of
  linguistics~7), 241--374. Berlin; Boston: de Gruyter Mouton.
\newblock OCLC: 949953027.

\bibitem[{Benjamini \& Hochberg(1995)}]{benjamini1995controlling}
Benjamini, Yoav \& Yosef Hochberg. 1995.
\newblock Controlling the false discovery rate: a practical and powerful
  approach to multiple testing.
\newblock \emph{Journal of the Royal Statistical Society. Series B
  (Methodological)} 289--300.

\bibitem[{Bickel(2013)}]{Bickel2013}
Bickel, Balthasar. 2013.
\newblock Distributional biases in language families.
\newblock In Balthasar Bickel, Lenore~A. Grenoble, David~A. Peterson \& Alan
  Timberlake (eds.), \emph{Language typology and historical contingency: In
  honor of {Johanna} {Nichols}}, 415--444. John Benjamins Publishing Company.
\newblock \doi{10.1075/tsl.104.19bic}.

\bibitem[{Bickel \& Nichols(2006)}]{bickel_oceania_2006}
Bickel, Balthasar \& Johanna Nichols. 2006.
\newblock Oceania, the {Pacific} {Rim}, and the theory of linguistic areas.
\newblock \emph{Proc. Berkeley Linguistics Society} 32. 3--15.

\bibitem[{Blevins(2006)}]{blevins_new_2006}
Blevins, Juliette. 2006.
\newblock New perspectives on {English} sound patterns: ``{Natural}'' and
  ``unnatural'' in {Evolutionary} {Phonology}.
\newblock \emph{Journal of English Linguistics} 34(1). 6--25.
\newblock \doi{10.1177/0075424206287585}.

\bibitem[{Campbell(2006)}]{campbell_areal_2006}
Campbell, Lyle. 2006.
\newblock Areal linguistics: {A} closer scrutiny.
\newblock In Yaron Matras, April McMahon \& Nigel Vincent (eds.),
  \emph{Linguistic areas: {Convergence} in historical and typological
  perspective}, 1--31. Basingstoke: Palgrave Macmillan.

\bibitem[{Campbell(2017)}]{campbell_why_2017}
Campbell, Lyle. 2017.
\newblock Why is it so hard to define a linguistic area?
\newblock In Raymond Hickey (ed.), \emph{The {Cambridge} handbook of areal
  linguistics}, 19--39. Cambridge: Cambridge University Press.
\newblock DOI: 10.1017/9781107279872.003.

\bibitem[{Chang et~al.(2015)Chang, Cathcart, Hall \& Garrett}]{Chang2015}
Chang, Will, Chundra Cathcart, David Hall \& Andrew Garrett. 2015.
\newblock Ancestry-constrained phylogenetic analysis supports the
  {Indo}-{European} steppe hypothesis.
\newblock \emph{Language} 91(1). 194--244.
\newblock \doi{10.1353/lan.2015.0005}.

\bibitem[{Clements(2003)}]{clements_feature_2003}
Clements, G.~N. 2003.
\newblock Feature economy in sound systems.
\newblock \emph{Phonology} 20(03). 287--333.
\newblock \doi{10.1017/S095267570400003X}.

\bibitem[{Cotterell \& Eisner(2017)}]{cotterell_probabilistic_2017}
Cotterell, Ryan \& Jason Eisner. 2017.
\newblock Probabilistic typology: Deep generative models of vowel inventories.
\newblock \emph{CoRR} abs/1705.01684.
\newblock \urlprefix\url{http://arxiv.org/abs/1705.01684}.

\bibitem[{Daum\'{e}~III(2009)}]{daume_iii_non-parametric_2009}
Daum\'{e}~III, Hal. 2009.
\newblock Non-parametric {Bayesian} areal linguistics.
\newblock In \emph{Proceedings of human language technologies: {The} 2009
  annual conference of the north american chapter of the association for
  computational linguistics}, 593--601. Association for Computational
  Linguistics.

\bibitem[{Delaunay(1934)}]{Delaunay1934}
Delaunay, Boris. 1934.
\newblock Sur la sph\`ere vide. {A} la m\'emoire de {Georges} {Vorono\"\i}.
\newblock \emph{Bulletin de l'Acad\'emie des Sciences de l'URSS. Classe des
  sciences math\'ematiques et naturelles} 6. 793--800.

\bibitem[{Donohue \& Nichols(2011)}]{Donohue2011}
Donohue, Mark \& Johanna Nichols. 2011.
\newblock Does phoneme inventory size correlate with population size?
\newblock \emph{Linguistic Typology} 15(2).
\newblock \doi{10.1515/lity.2011.011}.

\bibitem[{Dryer(1989)}]{Dryer1989}
Dryer, Matthew~S. 1989.
\newblock Large linguistic areas and language sampling.
\newblock \emph{Studies in Language} 13(2). 257–292.

\bibitem[{Dryer \& Haspelmath(2013)}]{wals}
Dryer, Matthew~S. \& Martin Haspelmath (eds.). 2013.
\newblock \emph{Wals online}.
\newblock Leipzig: Max Planck Institute for Evolutionary Anthropology.
\newblock \urlprefix\url{http://wals.info/}.

\bibitem[{Dunbar \& Dupoux(2016)}]{dunbar_geometric_2016}
Dunbar, Ewan \& Emmanuel Dupoux. 2016.
\newblock Geometric constraints on human speech sound inventories.
\newblock \emph{Frontiers in Psychology} 7.
\newblock \doi{10.3389/fpsyg.2016.01061}.

\bibitem[{Ember \& Ember(2007)}]{Ember2007}
Ember, Carol~R. \& Melvin Ember. 2007.
\newblock Climate, econiche, and sexuality: Influences on sonority in language.
\newblock \emph{American Anthropologist} 109(1). 180--185.
\newblock \doi{10.1525/aa.2007.109.1.180}.

\bibitem[{Emeneau(1956)}]{emeneau_india_1956}
Emeneau, Murray~B. 1956.
\newblock India as a {Lingustic} {Area}.
\newblock \emph{Language} 32(1). 3--16.
\newblock \doi{10.2307/410649}.

\bibitem[{Enfield(2005)}]{enfield_areal_2005}
Enfield, Nick~J. 2005.
\newblock Areal {Linguistics} and {Mainland} {Southeast} {Asia}.
\newblock \emph{Annual Review of Anthropology} 34(1). 181--206.
\newblock \doi{10.1146/annurev.anthro.34.081804.120406}.

\bibitem[{Everett(2013)}]{Everett2013}
Everett, Caleb. 2013.
\newblock Evidence for direct geographic influences on linguistic sounds: The
  case of ejectives.
\newblock \emph{{PLoS} {ONE}} 8(6). e65275.
\newblock \doi{10.1371/journal.pone.0065275}.

\bibitem[{Fortescue(1998)}]{Fortescue1998}
Fortescue, Michael. 1998.
\newblock \emph{Language relations across bering strait: reappraising the
  archaeological and linguistic evidence}.
\newblock London; New York: Cassell.

\bibitem[{Fought et~al.(2004)Fought, Munroe, Fought \& Good}]{Fought2004}
Fought, John~G., Robert~L. Munroe, Carmen~R. Fought \& Erin~M. Good. 2004.
\newblock Sonority and climate in a world sample of languages: Findings and
  prospects.
\newblock \emph{Cross-Cultural Research} 38(1). 27--51.
\newblock \doi{10.1177/1069397103259439}.

\bibitem[{Friedman(2000)}]{friedman_after_2000}
Friedman, Victor~A. 2000.
\newblock After 170 years of {Balkan} {Linguistics}: {Whither} the
  {Millennium}?
\newblock \emph{Mediterranean Language Review} 12. 1--15.

\bibitem[{Gordon(2016)}]{gordon2016phonological}
Gordon, Matthew. 2016.
\newblock \emph{Phonological typology}.
\newblock Oxford, United Kingdom: Oxford University Press.

\bibitem[{G\"{u}ldemann(2008)}]{guldemann_macro-sudan_2008}
G\"{u}ldemann, Tom. 2008.
\newblock The {Macro}-{Sudan} belt: towards identifying a linguistic area in
  northern sub-{Saharan} {Africa}.
\newblock In Bernd Heine \& Derek Nurse (eds.), \emph{A linguistic geography of
  {Africa}}, 151--185. Cambridge: Cambridge University Press.

\bibitem[{Hay \& Bauer(2007)}]{Hay2007}
Hay, Jennifer. \& Laurie Bauer. 2007.
\newblock Phoneme inventory size and population size.
\newblock \emph{Language} 83(2). 388--400.
\newblock \doi{10.1353/lan.2007.0071}.

\bibitem[{Hockett(1955)}]{hockett_manual_1955}
Hockett, Charles~F. 1955.
\newblock \emph{A manual of phonology}.
\newblock Baltimore: Waverley Press.

\bibitem[{Jaeger et~al.(2011)Jaeger, Graff, Croft \& Pontillo}]{Jaeger2011}
Jaeger, T.~Florian, Peter Graff, William Croft \& Daniel Pontillo. 2011.
\newblock Mixed effect models for genetic and areal dependencies in linguistic
  typology.
\newblock \emph{Linguistic Typology} 15(2).
\newblock \doi{10.1515/lity.2011.021}.

\bibitem[{Janssen et~al.(2006)Janssen, Bickel \&
  Z\'{u}\~{n}iga}]{janssen_randomization_2006}
Janssen, Dirk~P, Balthasar Bickel \& Fernando Z\'{u}\~{n}iga. 2006.
\newblock Randomization tests in language typology.
\newblock \emph{Linguistic Typology} 10(3).
\newblock \doi{10.1515/LINGTY.2006.013}.

\bibitem[{Kalyan \& Donohue(2017)}]{kalyan_mapping_2017}
Kalyan, Siva \& Mark Donohue. 2017.
\newblock Mapping hotspots of morphosyntactic and phonological diversity. {A}
  paper presented at the 12th {Conference} of the {Association} for
  {Linguistic} {Typology}, Canberra, Australia.

\bibitem[{Ladd et~al.(2015)Ladd, Roberts \& Dediu}]{Ladd2015}
Ladd, D.~Robert, Se{\'{a}}n~G. Roberts \& Dan Dediu. 2015.
\newblock Correlational studies in typological and historical linguistics.
\newblock \emph{Annual Review of Linguistics} 1(1). 221--241.
\newblock \doi{10.1146/annurev-linguist-030514-124819}.

\bibitem[{Liljencrants \& Lindblom(1972)}]{liljencrants_numerical_1972}
Liljencrants, Johan \& Bj\"{o}rn Lindblom. 1972.
\newblock Numerical simulation of vowel quality systems: the role of perceptual
  contrast.
\newblock \emph{Language} 48(4). 839.
\newblock \doi{10.2307/411991}.

\bibitem[{Lindblom \& Maddieson(1988)}]{lindblom_maddieson_1988}
Lindblom, Bj\"{o}rn \& Ian Maddieson. 1988.
\newblock Phonetic universals in consonant systems.
\newblock In Victoria Fromkin, Larry~M. Hyman \& Charles~N. Li (eds.),
  \emph{Language, speech, and mind: studies in honour of {Victoria} {A}.
  {Fromkin}}, 62--78. London ; New York: Routledge : published in the USA in
  association with Routledge, Chapman and Hall.

\bibitem[{Maddieson \& Precoda(1992)}]{MaddiesonPrecoda1992}
Maddieson, Iam \& Kristin Precoda. 1992.
\newblock \emph{Upsid and phoneme (version 1.1)}.
\newblock Los Angeles: University of California at Los Angeles.

\bibitem[{Maddieson(1984)}]{maddieson1984patterns}
Maddieson, Ian. 1984.
\newblock \emph{Patterns of sounds}.
\newblock Cambridge: Cambridge University Press.

\bibitem[{Maddieson(1991)}]{Maddieson1991}
Maddieson, Ian. 1991.
\newblock Testing the universality of phonological generalizations with a
  phonetically specified segment database: Results and limitations.
\newblock \emph{Phonetica} 48(2-4). 193--206.
\newblock \doi{10.1159/000261884}.

\bibitem[{Michael et~al.(2014)Michael, Chang \& Stark}]{michael_exploring_2014}
Michael, Lev, Will Chang \& Tammy Stark. 2014.
\newblock Exploring {Phonological} {Areality} in the {Circum}-{Andean} {Region}
  {Using} a {Naive} {Bayes} {Classifier}.
\newblock \emph{Language Dynamics and Change} 4(1). 27--86.
\newblock \doi{10.1163/22105832-00401004}.

\bibitem[{Michael et~al.(2015)Michael, Stark, Clem \& Chang}]{saphon}
Michael, Lev, Tammy Stark, Emily Clem \& Will Chang (eds.). 2015.
\newblock \emph{South american phonological inventory database v1.1.4.}
\newblock Berkeley: University of California.
\newblock \urlprefix\url{http://linguistics.berkeley.edu/~saphon/}.

\bibitem[{Mielke(2008)}]{mielke_emergence_2008}
Mielke, Jeff. 2008.
\newblock \emph{The emergence of distinctive features} Oxford linguistics.
\newblock Oxford ; New York: Oxford University Press.

\bibitem[{Moran et~al.(2012)Moran, McCloy \& Wright}]{Moran2012}
Moran, Steven, Daniel McCloy \& Richard Wright. 2012.
\newblock Revisiting population size vs. phoneme inventory size.
\newblock \emph{Language} 88(4). 877--893.
\newblock \doi{10.1353/lan.2012.0087}.

\bibitem[{Moran et~al.(2014)Moran, McCloy \& Wright}]{phoible}
Moran, Steven, Daniel McCloy \& Richard Wright (eds.). 2014.
\newblock \emph{Phoible online}.
\newblock Leipzig: Max Planck Institute for Evolutionary Anthropology.
\newblock \urlprefix\url{http://phoible.org/}.

\bibitem[{Murawaki \& Yamauchi(2018)}]{murawaki_statistical_2018}
Murawaki, Yugo \& Kenji Yamauchi. 2018.
\newblock A statistical model for the joint inference of vertical stability and
  horizontal diffusibility of typological features.
\newblock \emph{Journal of Language Evolution} 3(1). 13--25.
\newblock \doi{10.1093/jole/lzx022}.
\newblock \urlprefix\url{https://academic.oup.com/jole/article/3/1/13/4810662}.

\bibitem[{Nettle(2012)}]{Nettle2012}
Nettle, D. 2012.
\newblock Social scale and structural complexity in human languages.
\newblock \emph{Philosophical Transactions of the {Royal} {Society} {B}:
  {Biological} Sciences} 367(1597). 1829--1836.
\newblock \doi{10.1098/rstb.2011.0216}.

\bibitem[{Nichols(1992)}]{nichols1992}
Nichols, Johanna. 1992.
\newblock \emph{Linguistic diversity in space and time}.
\newblock University of Chicago Press.

\bibitem[{Nikolaev et~al.(2015)Nikolaev, Nikulin \& Kukhto}]{eurphon}
Nikolaev, Dmitry, Andrey Nikulin \& Anton Kukhto (eds.). 2015.
\newblock \emph{The database of eurasian phonological inventories (beta
  version)}.
\newblock \urlprefix\url{http://eurasianphonology.info/}.

\bibitem[{Pedregosa et~al.(2011)Pedregosa, Varoquaux, Gramfort, Michel,
  Thirion, Grisel, Blondel, Prettenhofer, Weiss, Dubourg, Vanderplas, Passos,
  Cournapeau, Brucher, Perrot \& Duchesnay}]{scikit-learn}
Pedregosa, F., G.~Varoquaux, A.~Gramfort, V.~Michel, B.~Thirion, O.~Grisel,
  M.~Blondel, P.~Prettenhofer, R.~Weiss, V.~Dubourg, J.~Vanderplas, A.~Passos,
  D.~Cournapeau, M.~Brucher, M.~Perrot \& E.~Duchesnay. 2011.
\newblock Scikit-learn: Machine learning in {P}ython.
\newblock \emph{Journal of Machine Learning Research} 12. 2825--2830.

\bibitem[{Potter(2005)}]{potter_permutation_2005}
Potter, Douglas~M. 2005.
\newblock A permutation test for inference in logistic regression with small-
  and moderate-sized data sets.
\newblock \emph{Statistics in Medicine} 24(5). 693--708.
\newblock \doi{10.1002/sim.1931}.
\newblock \urlprefix\url{http://doi.wiley.com/10.1002/sim.1931}.

\bibitem[{da~Silva \& Tehrani(2016)}]{da_silva_comparative_2016}
da~Silva, Sara~Graça \& Jamshid~J. Tehrani. 2016.
\newblock Comparative phylogenetic analyses uncover the ancient roots of
  {Indo}-{European} folktales.
\newblock \emph{Royal {Society} Open Science} 3(1). 150645.
\newblock \doi{10.1098/rsos.150645}.
\newblock
  \urlprefix\url{http://rsos.royalsocietypublishing.org/lookup/doi/10.1098/rsos.150645}.

\bibitem[{Simon et~al.(2011)Simon, Friedman, Hastie \& Tibshirani}]{glmnet}
Simon, Noah, Jerome Friedman, Trevor Hastie \& Rob Tibshirani. 2011.
\newblock Regularization paths for cox's proportional hazards model via
  coordinate descent.
\newblock \emph{Journal of Statistical Software} 39(5). 1--13.
\newblock \urlprefix\url{http://www.jstatsoft.org/v39/i05/}.

\bibitem[{Stevens(1989)}]{stevens_quantal_1989}
Stevens, Kenneth~N. 1989.
\newblock On the quantal nature of speech.
\newblock \emph{Journal of phonetics} 17(1). 3--45.

\bibitem[{Thomason(2000)}]{Thomason2000}
Thomason, Sarah~Grey. 2000.
\newblock Linguistic areas and language history.
\newblock \emph{Studies in Slavic and General Linguistics} 28. 311--327.

\bibitem[{Towner et~al.(2012)Towner, Grote, Venti \& Mulder}]{Towner2012}
Towner, Mary~C., Mark~N. Grote, Jay Venti \& Monique~Borgerhoff Mulder. 2012.
\newblock Cultural macroevolution on neighbor graphs.
\newblock \emph{Human Nature} 23(3). 283--305.
\newblock \doi{10.1007/s12110-012-9142-z}.

\bibitem[{Trubetzkoy(1939)}]{trubetzkoy_grundzuge_1939}
Trubetzkoy, Nikolai~S. 1939.
\newblock \emph{Grundz\"{u}ge der {Phonologie}}.
\newblock Prague.

\bibitem[{Trudgill(2011)}]{trudgill2011}
Trudgill, Peter. 2011.
\newblock \emph{Sociolinguistic typology: Social determinants of linguistic
  complexity}.
\newblock Oxford University Press.

\bibitem[{Wichmann(2017)}]{wichmann_genealogical_2017}
Wichmann, S{\o}ren. 2017.
\newblock Genealogical {Classification} in {Historical} {Linguistics}.
\newblock In Mark Aronoff (ed.), \emph{Oxford {Research} {Encyclopedia} of
  {Linguistics}}, \urlprefix\url{http://linguistics.oxfordre.com/view/10.1093/
  acrefore/9780199384655.001.0001/acrefore-9780199384655-e-78}.

\bibitem[{Wichmann et~al.(2011)Wichmann, Rama \& Holman}]{Wichmann2011}
Wichmann, S{\o}ren, Taraka Rama \& Eric~W. Holman. 2011.
\newblock Phonological diversity, word length, and population sizes across
  languages: The {ASJP} evidence.
\newblock \emph{Linguistic Typology} 15(2).
\newblock \doi{10.1515/lity.2011.013}.

\bibitem[{Wichmann \& Holman(2009)}]{WichmannHolman2009}
Wichmann, Søren \& Eric~W. Holman. 2009.
\newblock \emph{Temporal stability of linguistic typological features}.
\newblock Lincom Europa.

\bibitem[{Yamauchi \& Murawaki(2016)}]{yamauchi2016}
Yamauchi, Kenji \& Yugo Murawaki. 2016.
\newblock Contrasting vertical and horizontal transmission of typological
  features.
\newblock In \emph{Proc. of {COLING} 2016, the 26th international conference on
  computational linguistics: Technical papers}, 836--846.

\end{thebibliography}
