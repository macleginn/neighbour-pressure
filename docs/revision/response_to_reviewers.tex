\documentclass[a4paper,11pt]{article}

\author{}
\title{Areal dependence of consonant inventories: response to reviewers}

\begin{document}

\maketitle
	
First of all, I would like to thank the editor and the reviewers for their insightful critique, which helped me look at the data from a new angle and improve my analytical methods.

Below are the responses to individual comments.

\begin{quote}
	Reviewer 1 and Reviewer 3 are both concerned that genealogical and areal influences are highly correlated, and the problem of separating them is not sufficiently addressed and/or acknowledged. The revision should either explain how this problem has been addressed, or acknowledge that this is a problem not fully addressed by the model described in this paper. Since the genealogical predictor only includes phylum, and phylum and neighbor are correlated, phylum appears to be a less fine-grained version of the neighbor predictor (see Review 3 in particular), so it is hard to draw conclusions about the relative importance of these two factors. We don't know whether a the neighbor-pressure metric would still improve on a more sophisticated genealogical predictor.
\end{quote}

Two steps were undertaken to address this shortcoming:

\begin{enumerate}
	\item Full genealogical information available in the datasets used for the analysis (i.e. both phylum and genus labels) was incorporated in the model.
	\item An additional section was added at the beginning of the paper directly assessing the influence of phylogeny vis-a-vis language contact on the overall similarity of phonological inventories.
\end{enumerate}

\begin{quote}
	All three reviewers are concerned about the choice of distance metric. The author should follow Reviewer 1's recommendation to engage more closely with Jaeger et al.'s (2011) approach, which is closely related to this paper but currently only referenced only in passing. The apparently arbitrary 1000km cutoff should be reconsidered or defended. 	
\end{quote}

A more detailed discussion of merits and drawbacks of different distance metrics was added. A justification for the 1000 kilometer cut-off distance was proposed.

\begin{quote}
	Reviewers 1 and 3 both thought the argument against Daum\'{e}'s phylogeny/area model was strange and unconvincing. The author might also find it useful to refer to some of the chapters in Hickey, Raymond (ed.) 2017. Cambridge Handbook of Areal Linguistics that are consistent with this paper's proposal.	
\end{quote}

The argument was retracted. As for the areal handbook, I have by now a quite comprehensive knowledge of its contents, and I must say that I was a bit disappointed by a nearly complete lack of discussion of statistical methods for uncovering areal structures.

\begin{quote}
	The reviewers identify a number of ways to improve clarity of presentation. Figures 2 and 3 need to be explained more fully (see Review 1). I was confused by the terms ``feature\_present'' and ``feature\_name'' in the regression formula because the text leads me to believe that they refer to feature combinations (\textasciitilde phonemes) rather than features. This all needs to be completely clear, and the relationship between the model and the way it is talked about in the text needs to be clear as well.

	I think some readers will be very interested in which phonemes were best predicted by genealogical and areal factors, but the lists of phonemes are currently presented as lists in four footnotes, separated from the descriptions of what they are lists of, and they are articulatory descriptions interspersed with IPA symbols. This makes it very hard to digest. I would encourage the author to arrange them in a consonant chart and find some way to visually indicate which of the four groups they belong to. It also seems like a missed opportunity not to show how the influence of genealogical factors and areal factors is related to frequency in the database, beyond the comment about the ten most extreme ones.
\end{quote}

A more detailed discussion of the segments whose distribution is most or least dependent on NPM is provided. Several maps were added showing frequencies, phylum membership, and geographical distributions of languages possessing these segments.

\begin{quote}
	I found the introductory paradigm to be a red herring. It—most likely unintentionally—reiterates a line of thinking that is logically flawed and comes across as not understanding statistics: of course, any statistical generalization will have counter-examples. And what would ``meaningful precision'' be? The only definition I came up with would refer to the amount of *explainable variance* in phonemic inventories (i.e., far from 100\% of the observable variance) that a model can capture. This is easily fixed: I recommend omitting this paragraph.	
\end{quote}

The paragraph was replaced.

\begin{quote}
	There would seem to be a third approach, described in Jaeger et al (2011): a simple model over migration or geographic distance. (or alternatively, one might group this proposal in the second class, with the network approach). This proposal is not at all discussed (although the paper is cited in another context). 

	Specifically, Jaeger et al 2011 propose to include a \textbf{single} predictor in the statistical data analysis to capture the effect of language contact. The predictor is the weighted average of the value of the linguistic variable across all other languages—where the impact of each language is weighted by (inverse of the exponential) distance from the target language. This predictor in turn has only 1 degree of freedom (if estimated for the variable of interest, or 0 DF if held constant across studies). Jaeger et al (2011) show that this approach captures almost all of the effect of language family, subfamily, genus, and country, plus additional variance.
\end{quote}

A more detailed discussion of the conceptual advantages of using the network approach over regressing on travel distance was added.

\begin{quote}
	I struggled seeing how the present study really directly speaks to Enfield's point. Perhaps I’m missing something, but is the point here really just that language contact matters? Why not just say that? I’m sure Enfield is the only person who said that.
\end{quote}

Discussion of Enfield's theory was removed from the paper.

\begin{quote}
	Please explain clearly how to read the density graphs. What does the size of the dots mean. This should be part of the caption and the main text.
\end{quote}

The density graphs were tangential to the main issue discussed in the paper and were removed.

The review also included a very useful critique of the mixed-effects models employed in the paper. The models were radically simplified, and the main points of contention were thus removed. More precisely, the following drawbacks were addressed:

\begin{enumerate}
	\item No likelihood-ratio testing for the significance of a random effect is performed.
	\item Dependence of a distribution of a segment on NPM is now measured as the significance of a fixed effect. A~separate model for every segment was fitted thus eliminating the need for a complex RE structure.
	\item Comparison of the predictive power of phylogenetic vs. language-contact model is performed using accuracy and ROC-AUC metrics and is thus more interpretable.
\end{enumerate}

\begin{quote}
	NPM and phylum aren't demonstrated to be independent. I don't imagine they are. Phylum and neighbourhood are closely related. Languages within phylum tend to be geographically nearby. In order to draw conclusions about which is a better predictor, they need to be. (For example: judging just by the map in Figure 1, the database appears to contain: Portuguese **and** Galician **and** Spanish **and** perhaps Leonese et al; Irish **and** Scots Gaelic **and** what appears to be Norn; I might be wrong about some of these, but I don't think there are enough languages in Eurasia that neighbourhood and phylum could fail to be closely related.)	
\end{quote}

Analysis of pairs of neighbouring and non-neighbouring languages belonging to the same phylum/genus or different phyla/genera was undertaken to investigate this issue. It was demonstrated that NPM is still a significant factor influencing structures of phonological inventories even when phylum and genus are controlled for.

\begin{quote}
	I agree with the framing in the conclusion of the paper: the conceptual differences between NPM and phylum are two. First, ``the true ... process of linguistic transmission take[s] place ... sign by sign''; in other words, NPM is strictly *finer-grained* than phylum, as it operates on the feature (here == segment) level. If we'd constructed PPM - ``phylum pressure metric'' as alluded to in 2.1.1 - rather than NPM, this would be strictly speaking the only difference between the two. PPM would give us a phoneme-by-phoneme account of how the degree of prevalence of a given phoneme in the phylum. The paper really should say what the ``phylum'' predictor is! But I've imputed that it doesn't contain phoneme-specific information (a fortiori, doesn't contain graded information about phoneme prevalence).
\end{quote}

In the original model, a combination of phylum and phoneme was used as a predictor. In the revised analysis, a separate model was fit for every phoneme. In both cases, the ``phylum'' predictor is essentially PPM, viz. it gives information on the prevalence of a phoneme in a particular phylum (as well as genus in the revised analysis).

\begin{quote}
	A lesser problem: the first statistical analysis (3.2.2) doesn't make sense to me. What does it add that the second analysis (3.2.3) doesn't tell us? Prediction is a good way to assess model performance; likelihood (ratio) isn't, because it risks overfitting.
\end{quote}

I regard prediction performance measured by means of a held-out test data-set and by means of significance tests as complementary. Likelihood-ratio tests do not take into account the possibility of overfitting, while performance measures do not take into account the internal variability of the data; therefore it is hard to estimate whether a hike in prediction accuracy is not due to an idiosyncrasy of the sample, on which we perform cross validation. Therefore it seems justified to check the importance of a set of a predictor using both types of tests.

\end{document}